# MoDispenserMechanics
<img align="right" width="200" height="200" src="favicons/favicon-256.png">

### A plugin inspired by the [Autonomous Activator](https://oldcofh.github.io/docs/thermal-expansion/devices/autonomous-activator/) from the [Thermal Expansion](https://oldcofh.github.io/docs/thermal-expansion/) mod

### Giving dispensers the ability to use tools and weapons as well as place blocks
<br><br><br>
## Features
- Block Place Mechanic - Where blocks in the dispenser are placed in front of the dispenser.
- Item Use Mechanic - Where items can be used on blocks as if a player clicked them.
- Auto Craft Mechanic - Where dispensers facing into crafting tables can craft a recipe stored in the dispensers' inventory. Taking items from containers adjacent to the dispenser.
- Tool Break Block Mechanic - Where Pickaxes, Axes and Shovels can be used to break blocks in front of the dispenser. Applying enchantments such as Silk Touch, Fortune and Efficiency.
- Weapon Attack Mechanic - Where Axes and Swords can attack entities in front of the Dispenser. Applying enchantments such as Fire Aspect, Looting and Sharpness.
- When a tools' durability is too low to perform its action without breaking, the dispenser can drop the tool for it to be collected (configurable)

## Requirements
- Minecraft 1.20
- Bukkit/Spigot/Paper server distribution

## Installation
1. Initially start your server to generate the required directories then stop it with `/stop`
2. Install **MoDispenserMechanics** into your `./plugins/` directory
3. Start your server.

## Building
1. Simply run `./gradlew build`
2. You wil find the jar(s) in `/build/libs/` specifically `MoDispenserMechanics-<ver>.jar`

