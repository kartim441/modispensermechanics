package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.event.DispenserPlaceBlockEvent;
import me.taucu.modispensermechanics.util.MyDirectionalPlaceContext;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.craftbukkit.inventory.CraftItemStack;

public class BlockPlaceMechanic extends DispenseMechanic {

    static long blocksPlaced = 0;

    @Override
    public void onDispense(DispenseContext ctx) {
        BlockPos targetPos = ctx.pos().relative(ctx.facing());

        ItemStack is;
        if ((is = callPlaceEvent(ctx, targetPos)) != null && is.getItem() instanceof BlockItem item) {
            BlockPlaceContext placeCtx = new MyDirectionalPlaceContext(ctx.level(), targetPos, ctx.facing(), is, ctx.facing().getOpposite());

            if (item.place(placeCtx).consumesAction()) {
                ctx.stack(is);
                playPlaceSound(ctx.level(), ctx.pos(), item, placeCtx);
                ctx.result(DispenseResult.SUCCESS);
                blocksPlaced++;
            }
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        if (handler.getItem() == Items.CHEST) {
            // chest dispense behavior drops item when it fails ;(
            handler.removeIf(m -> m.getDispenseObject() instanceof OptionalDispenseItemBehavior);
        }
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
    }

    public static void playPlaceSound(LevelAccessor level, BlockPos pos, BlockItem item, BlockPlaceContext ctx) {
        BlockState state = item.getBlock().getStateForPlacement(ctx);
        if (state != null) {
            // modeled off of ItemStack#useOn
            SoundType sound = state.getSoundType();
            level.playSound(null, pos,
                    sound.getPlaceSound(),
                    SoundSource.BLOCKS,
                    (sound.getVolume()  + 1.0F) / 2.0F,
                    sound.getPitch() * 0.8F);
        }
    }

    public static ItemStack callPlaceEvent(DispenseContext ctx, BlockPos targetPos) {
        if (fireDispenseEvents) {
            if (!ctx.level().getBlockState(targetPos).canBeReplaced()) return null;
            var block = CraftBlock.at(ctx.level(), ctx.pos());
            var item = CraftItemStack.asBukkitCopy(ctx.stack());
            var target = CraftBlock.at(ctx.level(), targetPos);
            DispenserPlaceBlockEvent e = new DispenserPlaceBlockEvent(block, item, target);
            if (callEvent(e)) {
                return CraftItemStack.asNMSCopy(e.getItem());
            } else {
                return null;
            }
        }
        return ctx.stack();
    }

    public static int metricsBlocksPlaced() {
        int placed = Math.max(0, (int) Math.min(Integer.MAX_VALUE, blocksPlaced));
        blocksPlaced = 0;
        return placed;
    }

}
