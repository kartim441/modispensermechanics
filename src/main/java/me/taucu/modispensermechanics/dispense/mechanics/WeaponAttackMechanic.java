package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.util.ItemStackUtils;
import me.taucu.modispensermechanics.util.config.ToolDurabilityOption;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayerManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.decoration.ArmorStand;
import net.minecraft.world.entity.monster.warden.Warden;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.phys.AABB;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.craftbukkit.block.CraftDispenser;
import org.bukkit.event.entity.EntityCombustByBlockEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class WeaponAttackMechanic extends DispenseMechanic {

    public static final NamespacedKey LAST_ITEM_USE_GAME_TIME_KEY = new NamespacedKey("modispensermechanics", "lastattack");

    public static final HashSet<EntityType<?>> DISABLED_ENTITIES = new HashSet<>();

    public static boolean doSweeping;
    public static boolean respectAttackSpeed = true;

    static double metricsDamageDealt = 0;

    @Override
    public void onDispense(DispenseContext ctx) {
        if (ToolDurabilityOption.use(this, ctx, 1)) {
            List<Entity> victims = ctx.level().getEntities((Entity) null,
                    new AABB(ctx.pos().relative(ctx.facing())).inflate(-0.01),
                    test -> test.isAttackable() && EntitySelector.NO_SPECTATORS.test(test) && !DISABLED_ENTITIES.contains(test.getType()));

            if (!victims.isEmpty() && callDispenseEvent(ctx)) {
                BlockPos pos = ctx.pos();
                double px = pos.getX() + 0.5, py = pos.getY() + 0.5, pz = pos.getZ() + 0.5;

                // modeled off of Player#attack
                float itemAttackDamage = (float) ItemStackUtils.calculateAttribute(ctx.stack(), EquipmentSlot.MAINHAND, Attributes.ATTACK_DAMAGE, 1.0D);

                float attackDamageSpeedScalar;
                if (respectAttackSpeed) {
                    long lastUsedTime = getPDC(ctx.dispenser()).getOrDefault(LAST_ITEM_USE_GAME_TIME_KEY, PersistentDataType.LONG, (long) -Integer.MAX_VALUE);
                    attackDamageSpeedScalar = ItemStackUtils.getAttackDamageScale(ctx.level().getGameTime(), lastUsedTime, ctx.stack(), 0.5F);
                } else {
                    attackDamageSpeedScalar = 1.0F;
                }

                // create fake player because Enchantments like Looting only work for Players for some reason(tm)
                FakePlayer player = FakePlayerManager.getFakePlayer(ctx.level(), px, py-1.5, pz);
                player.setGameType(GameType.SURVIVAL);
                player.getInventory().items.set(0, ctx.stack());
                player.getInventory().selected = 0;

                boolean hurt = false;

                if (victims.size() > 1) {
                    // sort for closest entity (do it properly by bounding box)
                    victims.sort(Comparator.comparingDouble(ent -> {
                        AABB aabb = ent.getBoundingBox();
                        return square(aabb.minX - px) + square(aabb.maxX - px)
                                + square(aabb.minY - py) + square(aabb.maxY - py)
                                + square(aabb.minZ - pz) + square(aabb.maxZ - pz);
                    }));
                }

                for (Entity victim : victims) {
                    LivingEntity lvic = victim instanceof LivingEntity living ? living : null;
                    // do not attack entities that shouldn't be attacked or dying living entities
                    if (lvic == null || !lvic.isDeadOrDying()) {
                        float lastHealth = 0;

                        // bug with wardens constantly angry
                        DamageSource damageSource = player.damageSources().playerAttack(victim instanceof Warden ? null : player);

                        // modeled off of Player#attack
                        float attackDamage = itemAttackDamage + EnchantmentHelper.getDamageBonus(ctx.stack(), victim.getType());
                        if (lvic != null) {
                            lastHealth = lvic.getHealth();
                        }

                        // modeled off of Player#attack
                        attackDamage *= 0.2F + attackDamageSpeedScalar * attackDamageSpeedScalar * 0.8F;
                        attackDamage *= attackDamageSpeedScalar;

                        if (fireDispenseEvents) {
                            // paper deprecated entity damage event constructors for removal, while no alternative exists in spigot.
                            @SuppressWarnings("removal")
                            EntityDamageByBlockEvent edbbe = new EntityDamageByBlockEvent(CraftBlock.at(ctx.level(), ctx.pos()), victim.getBukkitEntity(), EntityDamageEvent.DamageCause.ENTITY_ATTACK, attackDamage);
                            Bukkit.getPluginManager().callEvent(edbbe);
                            if (edbbe.isCancelled()) {
                                break;
                            }
                            attackDamage = (float) edbbe.getDamage();
                        }

                        // modeled off of Player#attack
                        int fireDuration = EnchantmentHelper.getFireAspect(player) * 4;

                        // keep track of old fireTicks and reapply if hurt() fails
                        int previousFireTicks = victim.getRemainingFireTicks();

                        // Apply fire before hurt() otherwise it may die without being cooked
                        if (fireDuration * 20 > previousFireTicks) {
                            if (fireDispenseEvents) {
                                EntityCombustByBlockEvent combustEvent = new EntityCombustByBlockEvent(CraftBlock.at(ctx.level(), pos), victim.getBukkitEntity(), fireDuration);
                                Bukkit.getPluginManager().callEvent(combustEvent);
                                if (!combustEvent.isCancelled()) {
                                    victim.igniteForTicks(combustEvent.getDuration() * 20);
                                }
                            } else {
                                victim.igniteForTicks(fireDuration * 20);
                            }
                        }

                        if (victim.hurt(damageSource, attackDamage)) {
                            hurt = true;

                            int knockback = EnchantmentHelper.getKnockbackBonus(player);
                            if (knockback > 0) {
                                if (lvic == null) {
                                    victim.push((-Mth.sin(player.getYRot() * 0.017453292F) * (float) knockback * 0.5F), 0.1D, (Mth.cos(player.getYRot() * 0.017453292F) * (float) knockback * 0.5F));
                                } else {
                                    lvic.knockback(knockback * 0.5D, px - victim.getX(), pz - victim.getZ());
                                }
                            }

                            // apply sweeping attack
                            if (doSweeping && attackDamageSpeedScalar > 0.9F && ctx.stack().getItem() instanceof SwordItem) {
                                // modeled off of Player#Attack
                                List<LivingEntity> entitiesToSweep = ctx.level().getEntitiesOfClass(LivingEntity.class, victim.getBoundingBox().inflate(1.0D, 0.25D, 1.0D));
                                if (!entitiesToSweep.isEmpty()) {
                                    float sweepingDamage = 1.0F + EnchantmentHelper.getSweepingDamageRatio(player) * attackDamage;
                                    for (LivingEntity sweptEnt : entitiesToSweep) {
                                        if (sweptEnt != victim && sweptEnt != player && (!(sweptEnt instanceof ArmorStand armorStand) || !armorStand.isMarker())) {
                                            lastHealth = sweptEnt.getHealth();

                                            float newSweepingDamage = sweepingDamage;
                                            if (fireDispenseEvents) {
                                                // paper deprecated entity damage event constructors for removal, while no alternative exists in spigot.
                                                @SuppressWarnings("removal")
                                                EntityDamageByBlockEvent edbbe = new EntityDamageByBlockEvent(CraftBlock.at(ctx.level(), ctx.pos()), sweptEnt.getBukkitEntity(), EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK, newSweepingDamage);
                                                Bukkit.getPluginManager().callEvent(edbbe);
                                                if (edbbe.isCancelled()) {
                                                    continue;
                                                }
                                                newSweepingDamage = (float) Math.min(Float.MAX_VALUE, edbbe.getDamage());
                                            }

                                            if (sweptEnt.hurt(player.damageSources().playerAttack(player).sweep(), newSweepingDamage)) {
                                                sweptEnt.knockback(0.4000000059604645D, px - sweptEnt.getX(), pz - sweptEnt.getZ());
                                                metricsDamageDealt += Math.max(0, lastHealth - sweptEnt.getHealth());
                                            }
                                        }
                                    }
                                }

                            }

                            metricsDamageDealt += lvic == null ? 1 : Math.max(0, lastHealth - lvic.getHealth());
                            break;
                        } else {
                            // if we cannot hurt the entity apply the previousFireTicks of the entity
                            victim.setRemainingFireTicks(previousFireTicks);
                        }

                    }
                }

                // reset player inventory
                player.postDispense();

                // set cooldown for next attack
                if (respectAttackSpeed) {
                    getPDC(ctx.dispenser()).set(LAST_ITEM_USE_GAME_TIME_KEY, PersistentDataType.LONG, ctx.level().getGameTime());
                }

                // if we hurt something
                if (hurt) {
                    // damage weapon if required
                    if (ToolDurabilityOption.get() != ToolDurabilityOption.UNBREAKABLE && ItemStackUtils.hurtAndBreak(1, ctx.stack(), ctx.level().random)) {
                        ctx.level().playSound(null, pos, SoundEvents.ITEM_BREAK, SoundSource.BLOCKS, 1, 1);
                    }
                    ctx.result(DispenseResult.SUCCESS);
                    // play strong attack sound if attackDamageSpeedScalar > 0.9F
                    ctx.alwaysHandleSound(ctx2 -> playDispenseAttackSound(ctx2.level(), ctx2.pos(), attackDamageSpeedScalar > 0.9F));
                    return;
                } else {
                    ctx.alwaysHandleSound(ctx2 -> playDispenseAttackSound(ctx2.level(), ctx2.pos(), false));
                }
            }
            // always show particles to warn people that this dispenser means business
            ctx.alwaysHandleAnimation(this::playAnimation);
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
    }

    @Override
    public void playAnimation(DispenseContext ctx) {
        // always show particles to warn people that this dispenser means business
        playDispenseAnimation(ctx);
    }

    @Override
    public void playSound(DispenseContext ctx) {
        if (ctx.isResult(DispenseResult.SUCCESS)) {
            playDispenseAttackSound(ctx.level(), ctx.pos(), true);
            playDispenseSuccessSound(ctx.level(), ctx.pos());
        } else if (ctx.isResult(DispenseResult.FAILURE)) {
            playDispenseFailSound(ctx.level(), ctx.pos());
        }
    }

    public static void playDispenseAttackSound(ServerLevel level, BlockPos pos, boolean strongAttack) {
        level.playSeededSound(
                null,
                pos.getX(), pos.getY(), pos.getZ(),
                strongAttack ? SoundEvents.PLAYER_ATTACK_STRONG : SoundEvents.PLAYER_ATTACK_NODAMAGE,
                SoundSource.BLOCKS,
                1.0F, 1.0F,
                level.getRandom().nextLong()
        );
    }

    public static double square(double a) {
        return a * a;
    }

    @SuppressWarnings("UnreachableCode")
    public static PersistentDataContainer getPDC(DispenserBlockEntity dispenser) {
        PersistentDataContainer pdc = dispenser.persistentDataContainer;
        // PDC can be null on spigot/bukkit if it's never been used
        if (pdc == null) {
            pdc = (new CraftDispenser(dispenser.getLevel().getWorld(), dispenser)).getPersistentDataContainer();
        }
        return pdc;
    }

    public static int metricsDamageDealt() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, metricsDamageDealt));
        metricsDamageDealt = 0;
        return i;
    }

}
