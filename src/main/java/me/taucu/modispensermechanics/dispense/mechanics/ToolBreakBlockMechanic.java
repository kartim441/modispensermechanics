package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.dispense.tasks.ToolBreakBlockTask;
import me.taucu.modispensermechanics.event.DispenserBreakBlockEvent;
import me.taucu.modispensermechanics.event.DispenserStartBreakingBlockEvent;
import me.taucu.modispensermechanics.util.CollectionUtil;
import me.taucu.modispensermechanics.util.config.ToolDurabilityOption;
import net.minecraft.core.BlockPos;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.DiggerItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShearsItem;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Dispenser;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.craftbukkit.inventory.CraftItemStack;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Function;

public class ToolBreakBlockMechanic extends DispenseMechanic {

    private static boolean ENABLED = false;

    public static final boolean TILE_SNAPSHOTS_SUPPORTED = checkForTileSnapshotsSupport();
    private static Function<Chunk, org.bukkit.block.BlockState[]> getTilesMethod;

    public static final HashSet<Block> disabledBlocks = new HashSet<>();
    public static final HashSet<Item> disabledTools = new HashSet<>();

    public static boolean noopOnFailure;

    static {
        // bukkit support
        try {
            Chunk.class.getDeclaredMethod("getTileEntities", boolean.class);
            getTilesMethod = chunk -> chunk.getTileEntities(false);
        } catch (Throwable ignored) {
            getTilesMethod = Chunk::getTileEntities;
        }
    }

    public ToolBreakBlockMechanic() {
        setPriority(-50);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        BlockPos targetPos = ctx.pos().relative(ctx.facing());
        BlockState targetState = ctx.level().getBlockState(targetPos);

        if (disabledBlocks.contains(targetState.getBlock())) return;
        if (isBreakable(ctx.level(), targetPos, targetState, ctx.stack()) && callStartBreakingEvent(ctx, targetPos)) {
            if (ToolDurabilityOption.use(this, ctx, targetState)) {
                new ToolBreakBlockTask(ctx.dispenser(), targetState, targetPos, ctx.stack())
                        .start();
                ctx.result(DispenseResult.SUCCESS);
            } else {
                ctx.result(DispenseResult.FAILURE);
                if (ToolDurabilityOption.consumes()) {
                    dispenseIfTool(ctx.dispenser());
                }
            }
        } else {
            if (noopOnFailure) {
                ctx.defaultHandler(this);
            } else {
                onDefault(ctx);
            }
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
    }

    @Override
    public void onDefault(DispenseContext ctx) {
        ctx.result(DispenseResult.FAILURE);
        rearmDispenser(ctx.dispenser());
    }

    @Override
    public void playAnimation(DispenseContext ctx) {
        // we play the animation and sound from the ToolBreakBlockTask instead
    }

    @Override
    public void playSound(DispenseContext ctx) {
        // no fail clicky noises please ;)
        // unless someone else is trying to animate, then it would make sense
        if (ctx.alwaysHandleAnimation() != null) {
            super.playSound(ctx);
        }
    }

    public static boolean isBreakable(BlockGetter level, BlockPos pos, BlockState state, ItemStack stack) {
        return !(state instanceof GameMasterBlock
                || state.isAir()
                || state.getBlock() instanceof LiquidBlock
                || state.getDestroySpeed(level, pos) < 0F)
                && stack.getDestroySpeed(state) > 0F;
    }

    public static boolean callStartBreakingEvent(DispenseContext ctx, BlockPos targetPos) {
        if (fireDispenseEvents) {
            var e = new DispenserStartBreakingBlockEvent(
                    CraftBlock.at(ctx.level(), ctx.pos()),
                    CraftItemStack.asCraftMirror(ctx.stack()),
                    CraftBlock.at(ctx.level(), targetPos)
            );
            return callEvent(e);
        }
        return true;
    }

    public static List<ItemStack> callBreakEvent(Level level, BlockPos dispenserPos, ItemStack tool, BlockPos targetPos, List<ItemStack> drops, int[] expToDrop) {
        if (fireDispenseEvents) {
            var e = new DispenserBreakBlockEvent(
                    CraftBlock.at(level, dispenserPos),
                    CraftItemStack.asBukkitCopy(tool),
                    CraftBlock.at(level, targetPos),
                    CollectionUtil.map(drops, new ArrayList<>(), CraftItemStack::asCraftMirror),
                    expToDrop[0]
            );
            if (callEvent(e)) {
                expToDrop[0] = e.getExpToDrop();
                return CollectionUtil.map(e.getDrops(), new ArrayList<>(), CraftItemStack::asNMSCopy);
            } else {
                return null;
            }
        }
        return drops;
    }

    public static void rearmDispenser(DispenserBlockEntity dispenser) {
        // untrigger the dispenser without causing a block update so that the next update can re-trigger it
        dispenser.getLevel().setBlock(dispenser.getBlockPos(), dispenser.getBlockState().setValue(DispenserBlock.TRIGGERED, false), 2 | 16 | 1024, 0);
    }

    public static int getValidTool(DispenserBlockEntity dispenser) {
        int size = dispenser.getContainerSize();
        for (int i = 0; i < size; i++) {
            ItemStack stack = dispenser.getItem(i);
            if (stack != null && isTool(stack.getItem())) {
                return i;
            }
        }
        return -1;
    }

    public static boolean isTool(Item item) {
        return (item instanceof DiggerItem || item instanceof ShearsItem) && !disabledTools.contains(item);
    }

    public static boolean dispenseIfTool(DispenserBlockEntity dispenser) {
        int nextSlot = ToolBreakBlockMechanic.getValidTool(dispenser);
        if (nextSlot != -1) {
            ToolBreakBlockMechanic.dispense(dispenser, nextSlot);
            return true;
        }
        return false;
    }

    public static void dispense(DispenserBlockEntity dispenser, int nextSlot) {
        ItemStack stack = dispenser.getItem(nextSlot);
        if (stack != null) {
            DispenseItemHandler handler = MoDispenserMechanics.getInstance().getManager().getMechanicsFor(stack.getItem());
            for (DispenseMechanic mechanic : handler) {
                if (mechanic instanceof ToolBreakBlockMechanic) {
                    DispenseContext ctx = new DispenseContext(new BlockSource((ServerLevel) dispenser.getLevel(), dispenser.getBlockPos(), dispenser.getBlockState(), dispenser), dispenser, stack, nextSlot);
                    mechanic.onDispense(ctx);
                    if (ctx.isResult(DispenseResult.NOOP)) {
                        rearmDispenser(dispenser);
                    }
                    return;
                }
            }
        }
    }

    public static void setEnabled(boolean enabled) {
        if (enabled) {
            ENABLED = true;
            restartAll();
        } else {
            ENABLED = false;
            stopAll();
        }
    }

    public static boolean isEnabled() {
        return ENABLED;

    }
    public static void stopAll() {
        for (var task : ToolBreakBlockTask.getActiveTasks()) {
            task.stop();
        }
    }

    public static void restartAll() {
        stopAll();
        for (var w : Bukkit.getWorlds()) {
            processWorldLoad(w);
        }
    }

    public static void processWorldLoad(World w) {
        for (Chunk chunk : w.getLoadedChunks()) {
            processChunk(chunk);
        }
    }

    public static void processChunk(Chunk chunk) {
        if (ENABLED) {
            for (org.bukkit.block.BlockState state : getTilesMethod.apply(chunk)) {
                if (state instanceof Dispenser dispenser && dispenser.getPersistentDataContainer().has(ToolBreakBlockTask.BREAK_TASK_KEY, PersistentDataType.BYTE_ARRAY)) {
                    ToolBreakBlockTask.restart(dispenser);
                }
            }
        }
    }

    public static void processChunkUnload(Chunk chunk) {
        if (ENABLED) {
            for (org.bukkit.block.BlockState state : getTilesMethod.apply(chunk)) {
                if (state instanceof Dispenser dispenser) {
                    ToolBreakBlockTask.saveAndStop(dispenser);
                }
            }
        }
    }

    public static boolean checkForTileSnapshotsSupport() {
        try {
            org.bukkit.block.Dispenser.class.getMethod("isSnapshot");
            return true;
        } catch (Throwable ignored) {
            return false;
        }
    }

}
