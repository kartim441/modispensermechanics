package me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.util.DispenserUtil;
import me.taucu.modispensermechanics.util.InventoryUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.inventory.TransientCraftingContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.CraftingTableBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.bukkit.inventory.InventoryView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AutoCraftMechanic extends DispenseMechanic {

    public static final CraftingContainer CRAFTING_CONTAINER = createCraftingContainer();

    protected static long itemsCrafted = 0;

    public AutoCraftMechanic() {
        // we are very specific so we should handle everything first
        setPriority(100);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        BlockPos tablePos = ctx.pos().relative(ctx.facing());
        BlockState tableState = ctx.level().getBlockState(tablePos);
        if (tableState.getBlock() instanceof CraftingTableBlock && callDispenseEvent(ctx)) {
            // if a crafting table is in front of the dispenser we should handle it exclusively
            ctx.result(DispenseResult.FAILURE);

            BlockPos dispenserPos = ctx.pos();
            DispenserBlockEntity dispenser = ctx.dispenser();

            int craftingSize = dispenser.getContainerSize();
            List<ItemStack> dispenserContents = dispenser.getContents();
            List<ItemStack> craftingContents = CRAFTING_CONTAINER.getContents();

            int remainingIngredients = 0;

            Item[] itemsToFind = new Item[craftingSize];
            // add dispenser contents to get recipe
            for (int i = 0; i < craftingSize; i++) {
                ItemStack is = dispenserContents.get(i);
                if (!is.isEmpty()) {
                    itemsToFind[i] = is.getItem();
                    remainingIngredients++;
                }
                craftingContents.set(i, is);
            }
            // get the CachedRecipe present in the crafting inventory.
            CachedRecipe cachedRecipe = CachedRecipe.getRecipeFor(CRAFTING_CONTAINER, ctx.level());
            if (cachedRecipe == null) return; // if null it is an invalid recipe

            // clear the dispensers items from the craftingContents before we fill it with items from sources
            craftingContents.clear();

            // used to update neighboring comparators if contents change
            List<Container> connectedSources = new ArrayList<>();
            // search for items
            dirLoop: for (Direction dir : Direction.values()) {
                if (dir != ctx.facing()) {
                    BlockPos pos = dispenserPos.relative(dir);
                    // special case: don't take items from dispensers
                    if (ctx.level().getBlockEntity(pos) instanceof Container storage && !(storage instanceof DispenserBlockEntity)) {
                        Container[] connectedStorage = InventoryUtil.getConnectedContainers(storage);
                        boolean addToSources = true;
                        for (Container cont : connectedStorage) {
                            int size = cont.getContainerSize();
                            List<ItemStack> storageContents = cont.getContents();
                            // storage contents
                            for (int sp = 0; sp < size; sp++) {
                                ItemStack storageStack = storageContents.get(sp);
                                if (!storageStack.isEmpty()) {
                                    // used to keep track of how many items we will be taking from an ItemStack before we take them
                                    int stackCount = storageStack.getCount();
                                    // crafting contents
                                    for (int cp = 0; cp < craftingSize && stackCount > 0; cp++) {
                                        Item itemToFind = itemsToFind[cp];
                                        if (itemToFind != null && storageStack.is(itemToFind)) {
                                            // set the itemsToFind at this position to null, so we don't check it again
                                            itemsToFind[cp] = null;
                                            // set the ItemStack reference to the storages' ItemStack as we'll be modifying this item later on
                                            craftingContents.set(cp, storageStack);
                                            // so that we know we've already taken one of these items
                                            stackCount--;

                                            // used to update nearby comparators if contents change
                                            if (addToSources) {
                                                Collections.addAll(connectedSources, connectedStorage);
                                                addToSources = false;
                                            }

                                            // if remainingIngredients are zero, we know that we have everything so break out early
                                            if (--remainingIngredients < 1) break dirLoop;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // make sure we have no remaining items and then if the recipe matches
            if (remainingIngredients == 0 && cachedRecipe.recipe().matches(CRAFTING_CONTAINER, ctx.level())) {
                // assemble the recipe
                ItemStack resultStack = cachedRecipe.recipe().assemble(CRAFTING_CONTAINER, ctx.level().registryAccess());

                List<ItemStack> resultStacks = new ArrayList<>();
                resultStacks.add(resultStack);

                int resultItemCount = resultStack.getCount();

                // some crafting recipes have remaining items after the crafting process.
                // for example a cake recipe uses milk buckets and empty buckets are returned
                for (ItemStack is : craftingContents) {
                    Item remaining = is.getItem().getCraftingRemainingItem();
                    // It is likely that this is replaced with a more dynamic solution but currently this is how the game figures it out
                    if (remaining != null) {
                        ItemStack remainingStack = remaining.getDefaultInstance();
                        resultStacks.add(remainingStack);
                        resultItemCount += remainingStack.getCount();
                    }
                }

                BlockPos outputPos = tablePos.relative(ctx.facing());
                // if there is a container in front of the crafting table, drop items into it
                if (ctx.level().getBlockEntity(outputPos) instanceof Container output) {
                    // can we fit all items into the output container?
                    if (InventoryUtil.canMergeItems(output, resultStacks, ctx.facing().getOpposite())) {
                        // take placement from storage before merging
                        // this works as takeItems() decrements the count by 1, and the ItemStacks are references from the storage container
                        takeItems(craftingContents, 1);

                        // merge result item(s) into output
                        Container[] connectedOutputs = InventoryUtil.mergeItems(output, resultStacks, ctx.facing().getOpposite());

                        // update comparators neighboring the storage items were taken from and added to
                        updateNeighboursForOutputSignal(connectedSources);
                        updateNeighboursForOutputSignal(connectedOutputs);

                        ctx.result(DispenseResult.SUCCESS);
                        itemsCrafted += resultItemCount;
                    }
                } else { // otherwise drop the items
                    // take placement from storage before dropping
                    takeItems(craftingContents, 1);

                    // pre-merge & drop items to reduce redundant entity construction
                    // IE: [bucket 1x, bucket 1x, bucket 1x] becomes [bucket 3x]
                    for (ItemStack is : InventoryUtil.mergeItems(resultStacks, 64)) {
                        if (!is.isEmpty()) DispenserUtil.dropItem(ctx.level(), tablePos, ctx.facing(), is);
                    }

                    // update comparators neighboring the storage items were taken from
                    updateNeighboursForOutputSignal(connectedSources);

                    ctx.result(DispenseResult.SUCCESS);
                    itemsCrafted += resultItemCount;
                }
            }

        }
    }

    public static List<ItemStack> getRemainingIngredients(List<ItemStack> stacks) {
        List<ItemStack> remainingStacks = new ArrayList<>();
        for (ItemStack stack : stacks) {
            Item remaining = stack.getItem().getCraftingRemainingItem();
            if (remaining != null) remainingStacks.add(remaining.getDefaultInstance());
        }
        return remainingStacks;
    }

    public static void takeItems(Iterable<ItemStack> stacks, int amt) {
        for (ItemStack stack : stacks) {
            if (!stack.isEmpty()) stack.shrink(amt);
        }
    }

    public static void updateNeighboursForOutputSignal(Iterable<Container> containers) {
        for (Container connectedOutput : containers) updateNeighbourForOutputSignal(connectedOutput);
    }

    public static void updateNeighboursForOutputSignal(Container... containers) {
        for (Container connectedOutput : containers) updateNeighbourForOutputSignal(connectedOutput);
    }

    public static void updateNeighbourForOutputSignal(Container container) {
        if (container instanceof BlockEntity be && be.hasLevel()) {
            be.getLevel().updateNeighbourForOutputSignal(be.getBlockPos(), be.getBlockState().getBlock());
        }
    }

    public static CraftingContainer createCraftingContainer() {
        AbstractContainerMenu container = new AbstractContainerMenu(null, -1) {
            @Override
            public InventoryView getBukkitView() {
                return null;
            }

            @Override
            public boolean stillValid(Player player) {
                return false;
            }

            @Override
            public ItemStack quickMoveStack(Player player, int slot) {
                return ItemStack.EMPTY;
            }
        };

        return new TransientCraftingContainer(container, 3, 3);
    }

    @Override
    public void playSound(DispenseContext ctx) {
        if (ctx.isResult(DispenseResult.SUCCESS)) {
            playDispenseCraftSound(ctx.level(), ctx.pos());
            playDispenseSuccessSound(ctx.level(), ctx.pos());
        } else {
            playDispenseFailSound(ctx.level(), ctx.pos());
        }
    }

    public static void playDispenseCraftSound(ServerLevel level, BlockPos pos) {
        level.playSeededSound(
                null,
                pos.getX(), pos.getY(), pos.getZ(),
                SoundEvents.LANTERN_STEP,
                SoundSource.BLOCKS,
                1.0F, 1.0F,
                level.getRandom().nextLong()
        );
    }

    @Override
    public void playAnimation(DispenseContext ctx) {
        if (ctx.isResult(DispenseResult.SUCCESS)) {
            playDispenseAnimation(ctx.level(), ctx.pos().relative(ctx.facing()), ctx.facing());
        }
    }

    public static int metricsItemsCrafted() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, itemsCrafted));
        itemsCrafted = 0;
        return i;
    }

}
