package me.taucu.modispensermechanics.dispense.mechanics;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.util.DispenserUtil;
import me.taucu.modispensermechanics.util.InventoryUtil;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayerManager;
import net.minecraft.commands.arguments.EntityAnchorArgument;
import net.minecraft.core.BlockPos;
import net.minecraft.core.component.DataComponents;
import net.minecraft.core.dispenser.BlockSource;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;

import java.util.HashMap;
import java.util.List;

public class ItemUseMechanic extends DispenseMechanic {

    public static long itemsUsed = 0;

    public static boolean interactFacingBlockFirst = true;
    public static double interactRange = 5.0;

    public static final HashMap<Block, BlockUseBehavior> BLOCK_USE_BEHAVIORS = new HashMap<>();
    private static final BlockUseBehavior DEFAULT_BLOCK_USE_BEHAVIOR = new BlockUseBehavior();
    public static final HashMap<Item, ItemUseBehavior> ITEM_USE_BEHAVIORS = new HashMap<>();
    private static final ItemUseBehavior DEFAULT_ITEM_USE_BEHAVIOR = new ItemUseBehavior();

    private boolean preventVanilla;

    public ItemUseMechanic(boolean preventVanilla) {
        setPriority(-60);
        this.preventVanilla = preventVanilla;
    }

    // And here I was thinking this would be a simple mechanic...
    @Override
    public void onDispense(DispenseContext ctx) {
        if (callDispenseEvent(ctx)) {
            ItemStack stack = ctx.stack();
            ItemUseBehavior itemBehavior = ITEM_USE_BEHAVIORS.getOrDefault(stack.getItem(), DEFAULT_ITEM_USE_BEHAVIOR);
            BlockSource src = ctx.source();
            BlockPos facingPos = ctx.pos().relative(ctx.facing());
            BlockState facingState = ctx.level().getBlockState(facingPos);
            VoxelShape facingShape = facingState.getShape(ctx.level(), facingPos);
            Vec3 rayStart = new Vec3(ctx.x() + (ctx.facing().getStepX() / 1.95D), ctx.y() + (ctx.facing().getStepY() / 1.95D), ctx.z() + (ctx.facing().getStepZ() / 1.95D));
            FakePlayer player = FakePlayerManager.getFakePlayer(ctx.level(), rayStart.x(), rayStart.y() - 1.62D, rayStart.z());

            Inventory playerInv = player.getInventory();
            playerInv.items.set(0, stack);
            playerInv.selected = 0;

            BlockHitResult hit;
            // interact with anything in front of the dispenser if the facingShape is empty
            // otherwise look at the facingShape
            if (!interactFacingBlockFirst || facingShape.isEmpty()) {
                double interactRange = ItemUseMechanic.interactRange - (1 / 1.95D); // account for player offset
                // create a vector in the dispensers facing direction with a magnitude of interactDistance
                Vec3 rayEnd = new Vec3(rayStart.x() + (ctx.facing().getStepX() * interactRange), rayStart.y() + (ctx.facing().getStepY() * interactRange), rayStart.z() + (ctx.facing().getStepZ() * interactRange));

                // make the player look in the dispensers facing direction
                player.lookAt(EntityAnchorArgument.Anchor.EYES, rayEnd);

                hit = ray(rayStart, rayEnd, ctx, player, stack.getItem() instanceof BucketItem bucket && bucket.content == Fluids.EMPTY);
            } else {
                // make the player look at the center of the target voxel shape
                Vec3 aabbCenter = facingShape.toAabbs().getFirst().getCenter().add(facingPos.getX(), facingPos.getY(), facingPos.getZ());

                player.lookAt(EntityAnchorArgument.Anchor.EYES, aabbCenter);

                hit = ray(rayStart, aabbCenter, ctx, player, stack.getItem() instanceof BucketItem bucket && bucket.content == Fluids.EMPTY);
            }

            BlockState targetState;
            if (hit.getType() == HitResult.Type.MISS) {
                hit = hit.withPosition(facingPos).withDirection(ctx.facing().getOpposite());
                targetState = ctx.level().getBlockState(facingPos);
            } else {
                targetState = ctx.level().getBlockState(hit.getBlockPos());
            }

            BlockUseBehavior blockBehavior = BLOCK_USE_BEHAVIORS.getOrDefault(targetState.getBlock(), DEFAULT_BLOCK_USE_BEHAVIOR);
            if (!blockBehavior.disabled) {
                // always assume fake player is spectator
                player.setGameType(GameType.SURVIVAL);
                // modeled off of ServerPlayerGameMode#useItemOn and ServerPlayerGameMode#useItem
                player.setSecondaryUseActive(blockBehavior.sneaking);
                InteractionResult result;
                if (blockBehavior.sneaking || !(itemBehavior.itemStackUseOn && (result = targetState.useItemOn(stack, ctx.level(), player, InteractionHand.MAIN_HAND, hit).result()).consumesAction())) {
                    if (blockBehavior.sneaking || !(itemBehavior.blockStateUse && (result = targetState.useWithoutItem(ctx.level(), player, hit)).consumesAction())) {
                        if (itemBehavior.itemStackUse) {
                            InteractionResultHolder<ItemStack> resultHolder = stack.use(ctx.level(), player, InteractionHand.MAIN_HAND);
                            result = resultHolder.getResult();
                            playerInv.items.set(0, resultHolder.getObject());
                        } else {
                            result = InteractionResult.PASS;
                        }
                    }
                }
                player.setSecondaryUseActive(false);
                if (player.getUseItem().getComponents().has(DataComponents.FOOD)) {
                    player.stopUsingItem();
                } else {
                    player.finishUsingItem();
                }

                if (result.consumesAction()) {
                    if (player.containerMenu != null && player.containerMenu != player.inventoryMenu) {
                        player.doCloseContainer();
                    }

                    // remove one item from dispenser player
                    ctx.stack(playerInv.items.set(0, ItemStack.EMPTY));

                    // drop any excess items
                    if (!InventoryUtil.isEmpty(playerInv.items) || !InventoryUtil.isEmpty(playerInv.armor) || !InventoryUtil.isEmpty(playerInv.offhand)) {
                        // special case: Annoyingly, due to how dispensers are designed the ctx.stack() will be set after the logic returns.
                        // this means we can't merge items to that slot in the dispenser, or it'll be overwritten when the ctx.stack() used.
                        // so we must be sure to merge items of the same type until ctx.stack() is full and then make sure we don't merge to it.
                        DispenserBlockEntity dispenser = ctx.dispenser();
                        List<ItemStack> contents = dispenser.getContents();
                        List<ItemStack> toMerge = playerInv.getContents();

                        // see what we can merge with the ctx.stack() first
                        ItemStack ctxStack = ctx.stack();
                        ctx.stack(InventoryUtil.mergeItemsWithItem(ctxStack, toMerge, dispenser.getMaxStackSize()));

                        // now merge with every slot that isn't the dispensed slot
                        boolean empty = false;
                        for (int i = 0; i < contents.size(); i++) {
                            if (i != ctx.slot() && InventoryUtil.mergeItemsWithSlot(contents, i, toMerge, dispenser.getMaxStackSize())) {
                                empty = true;
                            }
                        }

                        // if we didn't merge all items, drop the remaining items.
                        if (!empty) {
                            // if there is a full-ish block in front of the dispenser, drop the items the next block past it
                            BlockPos dropPos = ctx.pos();
                            if (facingPos.equals(hit.getBlockPos())) {
                                boolean insideSolidBlock = targetState.getCollisionShape(ctx.level(), hit.getBlockPos()).toAabbs().stream()
                                        .mapToDouble(AABB::getSize)
                                        .sum() > 0.5D;
                                if (insideSolidBlock) dropPos = dropPos.relative(ctx.facing());
                            }

                            for (ItemStack toDrop : toMerge) {
                                DispenserUtil.dropItem(ctx.level(), dropPos, ctx.facing(), toDrop);
                            }
                        }
                    }

                    ctx.result(DispenseResult.SUCCESS);
                    itemsUsed++;
                }
            }

            player.postDispense();
        }
    }

    @Override
    public void onPair(DispenseItemHandler handler) {
        handler.removeIf(m -> m.getDispenseClass() == DefaultDispenseItemBehavior.class);
        if (preventVanilla) handler.removeIf(m -> m.getDispenseClass().getPackageName().startsWith(DefaultDispenseItemBehavior.class.getPackageName()));
    }

    public static BlockHitResult ray(Vec3 rayStart, Vec3 direction, DispenseContext ctx, Entity ent, boolean includeFluids) {
        return ctx.level().clip(new ClipContext(
                rayStart,
                direction,
                ClipContext.Block.OUTLINE,
                includeFluids ? ClipContext.Fluid.SOURCE_ONLY : ClipContext.Fluid.NONE,
                ent));
    }

    public static Integer metricsItemsUsed() {
        int i = Math.max(0, (int) Math.min(Integer.MAX_VALUE, itemsUsed));
        itemsUsed = 0;
        return i;
    }

    public static class BlockUseBehavior {
        public boolean disabled = false;
        public boolean sneaking = false;
    }

    public static BlockUseBehavior getBehaviorFor(Block block) {
        return behaviorFor(block, false);
    }

    public static BlockUseBehavior behaviorFor(Block block, boolean add) {
        BlockUseBehavior behavior = BLOCK_USE_BEHAVIORS.get(block);
        if (behavior == null) {
            behavior = new BlockUseBehavior();
            if (add) {
                BLOCK_USE_BEHAVIORS.put(block, behavior);
            }
        }
        return behavior;
    }

    public static class ItemUseBehavior {
        public boolean enabled = true;
        public boolean preventVanilla = false;
        public boolean blockStateUse = true;
        public boolean itemStackUseOn = true;
        public boolean itemStackUse = true;
    }

    public static ItemUseBehavior getBehaviorFor(Item item) {
        return behaviorFor(item, false);
    }

    public static ItemUseBehavior behaviorFor(Item item, boolean add) {
        ItemUseBehavior behavior = ITEM_USE_BEHAVIORS.get(item);
        if (behavior == null) {
            behavior = new ItemUseBehavior();
            if (add) {
                ITEM_USE_BEHAVIORS.put(item, behavior);
            }
        }
        return behavior;
    }

}
