package me.taucu.modispensermechanics.dispense.wrappers;

import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.world.item.ItemStack;

public class OptionalDispenseBehaviorWrapper extends DispenseBehaviorWrapper {

    public OptionalDispenseBehaviorWrapper(OptionalDispenseItemBehavior behavior) {
        super(behavior);
    }

    @Override
    public void onDispense(DispenseContext ctx) {
        ItemStack stack = behavior.dispense(ctx.source(), ctx.stack());
        if (getDispenseObject().isSuccess()) {
            ctx.result(DispenseResult.SUCCESS);
            ctx.stack(stack);
        } else {
            ctx.result(DispenseResult.FAILURE);
        }
    }

    @Override
    public OptionalDispenseItemBehavior getDispenseObject() {
        return (OptionalDispenseItemBehavior) super.getDispenseObject();
    }

}
