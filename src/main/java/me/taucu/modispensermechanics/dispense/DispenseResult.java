package me.taucu.modispensermechanics.dispense;

public enum DispenseResult {
    SUCCESS,
    FAILURE,
    NOOP
}
