package me.taucu.modispensermechanics.dispense.tasks;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.ToolBreakBlockMechanic;
import me.taucu.modispensermechanics.scheduler.TickTask;
import me.taucu.modispensermechanics.util.BlockUtils;
import me.taucu.modispensermechanics.util.InventoryUtil;
import me.taucu.modispensermechanics.util.ItemStackUtils;
import me.taucu.modispensermechanics.util.config.ToolDurabilityOption;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayerManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientboundBlockDestructionPacket;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DispenserBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.DispenserBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Dispenser;
import org.bukkit.craftbukkit.block.CraftBlock;
import org.bukkit.craftbukkit.block.CraftDispenser;
import org.bukkit.persistence.PersistentDataType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

public class ToolBreakBlockTask extends TickTask {

    protected static final HashMap<DispenserBlockEntity, ToolBreakBlockTask> ACTIVE_TASKS = new HashMap<>();

    public static final NamespacedKey BREAK_TASK_KEY = new NamespacedKey("modispensermechanics", "blockbreaktask");
    public static final double DESTROY_PROGRESS_DISTANCE = 32.0D;
    public static final int DATA_VERSION = 1;

    public static float speedMultiplier = 1.0F;
    public static long blocksBroken = 0;

    protected static short nextBlockProgressId = 0;

    protected final ServerLevel level;
    protected final DispenserBlockEntity dispenser;

    protected final ItemStack toolStack;
    protected final int toolSlot;

    protected final BlockPos targetPos;
    protected final BlockState targetState;
    protected final SoundType targetSound;
    protected final float progressIncrement;

    protected boolean removeSave = false;

    protected int ticks = 0;

    protected int blockProgressId = 0;
    protected float destroyProgress = 0;

    protected int nextBreakStage = 0;
    protected int nextRefreshBreakStageTicks = 0;

    public ToolBreakBlockTask(DispenserBlockEntity dispenser, BlockState targetState, BlockPos targetPos, ItemStack stack) {
        this.level = (ServerLevel) dispenser.getLevel();
        this.dispenser = dispenser;
        this.toolStack = stack;
        this.toolSlot = InventoryUtil.indexOf(dispenser.getContents(), stack);

        this.targetPos = targetPos;
        this.targetState = targetState;
        this.targetSound = targetState.getSoundType();

        this.progressIncrement = BlockUtils.getDestroyRate(level, targetPos, targetState, stack) * speedMultiplier * 10F;
    }

    @Override
    public void tick() {
        if (!dispenser.isRemoved() && dispenser.getBlockState().getValue(DispenserBlock.TRIGGERED) && toolStack.is(dispenser.getItem(toolSlot).getItem())) {
            if (level.getBlockState(targetPos).is(targetState.getBlock())) {
                if (destroyProgress < 10F) {
                    destroyProgress += progressIncrement;
                    if (destroyProgress >= nextBreakStage || ticks >= nextRefreshBreakStageTicks) {
                        destroyBlockProgress(nextBreakStage, DESTROY_PROGRESS_DISTANCE);
                        nextBreakStage = (int) destroyProgress + 1;
                        nextRefreshBreakStageTicks = ticks + 20;
                    }
                    if (ticks % 4 == 0) {
                        playHitSound();
                    }
                } else {
                    stop(true);
                    breakBlock();
                }
                ticks++;
            } else {
                stop(true);
                ((DispenserBlock) dispenser.getBlockState().getBlock()).dispenseFrom(level, dispenser.getBlockState(), dispenser.getBlockPos());
                ToolBreakBlockMechanic.rearmDispenser(dispenser);
            }
            return;
        }
        stop(true);
    }

    public void breakBlock() {
        ItemStack stack = dispenser.getItem(toolSlot);

        // make sure the dispenser still has the exact same tool in it
        if (ItemStack.matches(toolStack, stack)) {
            boolean isCorrectTool = stack.isCorrectToolForDrops(targetState);
            BlockEntity targetTile = level.getBlockEntity(targetPos);

            int[] exp = {targetState.getBlock().getExpDrop(targetState, level, targetPos, stack, true)};
            List<ItemStack> drops = Block.getDrops(targetState, level, targetPos, targetTile, FakePlayerManager.getFakePlayer(level), stack);

            // Call events, break block & play effects
            if ((drops = ToolBreakBlockMechanic.callBreakEvent(level, dispenser.getBlockPos(), stack, targetPos, drops, exp)) != null && level.destroyBlock(targetPos, false)) {
                // I really don't like how you have to implement this. No wonder this game has so many dupes...
                // It seems Containers drop their contents when the block is set, having no bearing on dropResources,
                //   while things like Bees Nests drop their item with Bees by dropResources.
                // #BlameMojang moment - imo this is stupid.
                if (isCorrectTool || !targetState.requiresCorrectToolForDrops()) {
                    drops.forEach(is -> Block.popResource(level, targetPos, is));
                    targetState.spawnAfterBreak(level, targetPos, stack, false);

                    if (exp[0] > 0) {
                        targetState.getBlock().popExperience(level, targetPos, exp[0]);
                    }
                }

                // damage the item if we managed to break the block
                if (ToolDurabilityOption.get() != ToolDurabilityOption.UNBREAKABLE && ItemStackUtils.hurtAndBreak(isCorrectTool ? 1 : 2, stack, level.random)) {
                    level.playSound(null, dispenser.getBlockPos(), SoundEvents.ITEM_BREAK, SoundSource.BLOCKS, 1, 1);
                    ToolBreakBlockMechanic.dispenseIfTool(dispenser);
                } else {
                    // if the tool still has durability, rearm the dispenser for next use
                    ToolBreakBlockMechanic.rearmDispenser(dispenser);
                }

                // play dispense effects
                DispenseMechanic.playDispenseAnimation(level, dispenser.getBlockPos(), dispenser.getBlockState().getValue(DispenserBlock.FACING));
                DispenseMechanic.playDispenseSuccessSound(level, dispenser.getBlockPos());

                blocksBroken++;
            }
        } else {
            ToolBreakBlockMechanic.dispenseIfTool(dispenser);
        }
    }

    public void destroyBlockProgress(int progress, double distance) {
        // construct the packet and send it ourselves to avoid bukkits getEntity calls on level#destroyBlockProgress
        Packet<?> packet = new ClientboundBlockDestructionPacket(blockProgressId, targetPos, Math.min(9, progress));
        level.getServer().getPlayerList().broadcast(null, targetPos.getX(), targetPos.getY(), targetPos.getZ(), distance, level.dimension(), packet);
    }

    public void playHitSound() {
        level.playSound(null,
                targetPos.getX() + 0.5D,
                targetPos.getY() + 0.5D,
                targetPos.getZ() + 0.5D,
                targetSound.getHitSound(),
                SoundSource.BLOCKS,
                targetSound.getVolume() / 2,
                targetSound.getPitch());
    }

    @Override
    public void onStart() {
        this.blockProgressId = nextBlockProgressId();
        ToolBreakBlockTask check = ACTIVE_TASKS.put(dispenser, this);
        if (check != null && check.dispenser == dispenser) {
            check.stop(true);
        }
        // instant break
        if (progressIncrement >= 10F) destroyProgress = 10F;
        // tick because scheduled tasks will run one tick later
        tick();
    }

    public void stop(boolean removeSave) {
        this.removeSave = removeSave;
        stop();
    }

    @Override
    public void onStop() {
        ACTIVE_TASKS.remove(dispenser, this);
        // larger distance to ensure anyone who may have received a progress update know that it isn't being broken anymore
        destroyBlockProgress(-1, DESTROY_PROGRESS_DISTANCE * 1.5);
        save();
    }

    public void save() {
        try {
            if (removeSave) {
                if (dispenser.persistentDataContainer != null) {
                    dispenser.persistentDataContainer.remove(BREAK_TASK_KEY);
                }
                // in bukkit the persistent data container may not be initialized, so we have to get it the Bukkit way.
            } else if (ticks > 0 && CraftBlock.at(level, dispenser.getBlockPos()).getState() instanceof Dispenser craftDispenser) {
                CompoundTag tag = new CompoundTag();
                tag.putInt("version", DATA_VERSION);
                tag.putByte("slot", (byte) toolSlot);
                tag.putInt("ticks", ticks);

                ByteArrayOutputStream os = new ByteArrayOutputStream();
                NbtIo.write(tag, new DataOutputStream(os));
                craftDispenser.getPersistentDataContainer().set(BREAK_TASK_KEY, PersistentDataType.BYTE_ARRAY, os.toByteArray());
                craftDispenser.update(false ,false);
            }
        } catch (Exception e) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Exception caught while saving break block task", e);
        }
    }

    public void setBreakingTicks(int ticks) {
        this.ticks = ticks;
        this.destroyProgress = (progressIncrement * ticks);
    }

    public static void restart(org.bukkit.block.Dispenser bukkitDispenser) {
        try {
            CraftDispenser craftDispenser = (CraftDispenser) bukkitDispenser;

            byte[] arr = craftDispenser.getPersistentDataContainer().get(BREAK_TASK_KEY, PersistentDataType.BYTE_ARRAY);
            craftDispenser.getPersistentDataContainer().remove(BREAK_TASK_KEY);
            if (ToolBreakBlockMechanic.TILE_SNAPSHOTS_SUPPORTED && craftDispenser.isSnapshot()) {
                craftDispenser.update(false, false);
            }

            CompoundTag tag = NbtIo.read(new DataInputStream(new ByteArrayInputStream(arr)));
            int dataVersion = tag.getInt("version");
            if (dataVersion != DATA_VERSION) {
                if (dataVersion > DATA_VERSION) {
                    return;
                }
            }

            DispenserBlockEntity dispenser = (DispenserBlockEntity) craftDispenser.getWorldHandle().getBlockEntity(craftDispenser.getPosition());

            ItemStack stack = dispenser.getItem(tag.getByte("slot"));
            Item item = stack.getItem();
            if (ToolBreakBlockMechanic.isTool(item)) {
                Direction facing = craftDispenser.getHandle().getValue(DispenserBlock.FACING);
                BlockPos targetPos = dispenser.getBlockPos().relative(facing);
                BlockState targetState = dispenser.getLevel().getBlockState(targetPos);

                if (ToolBreakBlockMechanic.isBreakable(dispenser.getLevel(), targetPos, targetState, stack)) {
                    ToolBreakBlockTask task = new ToolBreakBlockTask(dispenser, targetState, targetPos, stack);
                    task.setBreakingTicks(tag.getInt("ticks"));

                    task.start();
                }
            } else {
                MoDispenserMechanics.getInstance().getLogger().warning("could not restart ToolBreakBlockTask at " + bukkitDispenser.getBlock() + " as the tool " + item.getDescriptionId() + " is not a valid tool");
            }
        } catch (Exception e) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while restarting ToolBreakBlockTask: " + bukkitDispenser.getBlock(), e);
        }
    }

    public static void saveAndStop(org.bukkit.block.Dispenser bukkitDispenser) {
        CraftDispenser craftDispenser = (CraftDispenser) bukkitDispenser;
        if (craftDispenser.getBlock().getHandle().getBlockEntity(craftDispenser.getPosition()) instanceof DispenserBlockEntity dispenser) {
            ToolBreakBlockTask task = ACTIVE_TASKS.get(dispenser);
            if (task != null) {
                task.stop();
            }
        }
    }

    public static int nextBlockProgressId() {
        // keep the progress ID negative to avoid conflicts with real entities; spinning the short as it overflows
        return -1 - (nextBlockProgressId++ & 0xFFFF);
    }

    public static List<ToolBreakBlockTask> getActiveTasks() {
        return new ArrayList<>(ACTIVE_TASKS.values());
    }

    public static int metricsBlocksBroken() {
        int broken = Math.max(0, (int) Math.min(Integer.MAX_VALUE, blocksBroken));
        blocksBroken = 0;
        return broken;
    }

}
