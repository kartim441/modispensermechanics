package me.taucu.modispensermechanics.scheduler;

public abstract class TickTask {

    int id = -1;

    public abstract void tick();

    public void onStart() {}

    public void onStop() {}

    public TickTask start() {
        return TaskTicker.start(this);
    }

    public TickTask stop() {
        return TaskTicker.stop(this);
    }

    public final boolean isRunning() {
        return TaskTicker.isRunning(this);
    }

}
