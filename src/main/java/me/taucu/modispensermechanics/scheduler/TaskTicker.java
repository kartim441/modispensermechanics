package me.taucu.modispensermechanics.scheduler;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.logging.Level;

public class TaskTicker {

    private static final int PADDING = 100;
    private static TickTask[] tasks = new TickTask[PADDING];
    private static int taskIndex = 0;
    private static int taskCount = 0;

    public static TickTask start(TickTask task) {
        if (isRunning(task)) {
            throw new IllegalStateException("already running");
        } else {
            if (tasks.length == taskIndex) {
                TickTask[] newTasks = new TickTask[tasks.length + PADDING];
                System.arraycopy(tasks, 0, newTasks, 0, tasks.length);
                tasks = newTasks;
            }

            tasks[taskIndex] = task;
            task.id = taskIndex++;
            taskCount++;

            task.onStart();
        }
        return task;
    }

    public static TickTask stop(TickTask task) {
        if (isRunning(task)) {
            tasks[task.id] = null;
            taskCount--;
            task.id = -1;
            task.onStop();
        }
        return task;
    }

    public static boolean isRunning(TickTask task) {
        return task.id >= 0 && tasks.length > task.id && tasks[task.id] == task;
    }

    public static void scheduleDelayed(Runnable run, int delay) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(MoDispenserMechanics.getInstance(), run, delay);
    }

    public static TickTask[] getTasks() {
        if (tasks.length != taskCount) {
            // remove nulls from array
            defragment(0);
        }
        return Arrays.copyOf(tasks, tasks.length);
    }

    public static void stopAll() {
        for (TickTask task : tasks) {
            if (task != null) {
                task.stop();
            }
        }
        defragment(0);
    }

    private static void heartbeat() {
        if (taskCount > 0) {
            int fragmentation = tickTasks();
            // if there is roughly 33% or more fragmentation we should defragment the array
            if (fragmentation > 20 + (taskCount/3)) {
                defragment(PADDING);
            }
        } else if (tasks.length > PADDING * 2) {
            defragment(PADDING);
        }
    }

    private static int tickTasks() {
        final TickTask[] currentTasks = tasks;
        // if the index of tasks we have ticked equal the last taskCount we know the rest is just padding/new tasks
        int maxPos = taskCount;
        int i = 0;
        TickTask task = null;
        while (i < maxPos) {
            // put `try` outside for better performance
            // the external loop will reset this one where it left off if an exception must be handled
            try {
                while (i < maxPos) {
                    task = currentTasks[i];
                    if (task == null) {
                        maxPos++;
                    } else {
                        task.tick();
                    }
                    i++;
                }
            } catch (Throwable t) {
                if (t instanceof ThreadDeath) {
                    throw t;
                }
                MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while ticking task: " + task, t);
                // goto next task
                i++;
            }
        }
        // amount of fragmented nulls in the taskCount
        return maxPos - taskCount;
    }

    private static void defragment(int padding) {
        TickTask[] newTasks = new TickTask[padding + taskCount];

        int newIndex = 0;
        for (TickTask task : tasks) {
            if (task != null) {
                newTasks[newIndex] = task;
                task.id = newIndex++;
            }
        }

        tasks = newTasks;
        taskIndex = newIndex;
    }

    public static void init() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(MoDispenserMechanics.getInstance(), TaskTicker::heartbeat, 0, 1);
    }

}
