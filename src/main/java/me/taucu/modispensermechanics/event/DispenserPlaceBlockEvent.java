package me.taucu.modispensermechanics.event;

import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class DispenserPlaceBlockEvent extends BlockDispenseEvent {

    private final Block target;

    public DispenserPlaceBlockEvent(@NotNull Block block, @NotNull ItemStack dispensed, @NotNull Block target) {
        super(block, dispensed, new Vector());
        this.target = target;
    }

    public Block getTargetBlock() {
        return target;
    }

    public static HandlerList getHandlerList() {
        return BlockDispenseEvent.getHandlerList();
    }

}
