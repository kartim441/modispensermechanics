package me.taucu.modispensermechanics.event;

import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DispenserBreakBlockEvent extends DispenserStartBreakingBlockEvent {

    private final List<ItemStack> drops;
    private int expToDrop;

    public DispenserBreakBlockEvent(@NotNull Block block, @NotNull ItemStack tool, @NotNull Block targetBlock, @NotNull List<ItemStack> drops, int expToDrop) {
        super(block, tool, targetBlock);
        this.drops = drops;
        this.expToDrop = expToDrop;
    }

    public List<ItemStack> getDrops() {
        return drops;
    }

    public int getExpToDrop() {
        return expToDrop;
    }

    public void setExpToDrop(int expToDrop) {
        this.expToDrop = expToDrop;
    }

    /**
     * Note: changing the item will have no effect on the result of this event
     */
    @Override
    public @NotNull ItemStack getItem() {
        return super.getItem();
    }

    /**
     * Note: changing the item will have no effect on the result of this event, nor will it update the item in the dispenser
     */
    @Override
    public void setItem(@NotNull ItemStack item) {
        super.setItem(item);
    }

    public static HandlerList getHandlerList() {
        return DispenserStartBreakingBlockEvent.getHandlerList();
    }

}
