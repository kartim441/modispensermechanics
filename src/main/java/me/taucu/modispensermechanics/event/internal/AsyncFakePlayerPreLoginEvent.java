package me.taucu.modispensermechanics.event.internal;

import com.destroystokyo.paper.profile.PlayerProfile;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerPreLoginEvent;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.util.UUID;

public class AsyncFakePlayerPreLoginEvent extends AsyncPlayerPreLoginEvent {

    public AsyncFakePlayerPreLoginEvent(@NotNull final String name, @NotNull final InetAddress ipAddress, @NotNull final UUID uniqueId) {
        super(name, ipAddress, uniqueId);
    }

    @Override
    public void setLoginResult(@NotNull Result result) {}

    @Override
    public void setPlayerProfile(@NotNull PlayerProfile profile) {}

    @Override
    @SuppressWarnings("deprecation")
    public void setKickMessage(@NotNull String message) {}

    @Override
    @SuppressWarnings("deprecation")
    public void setResult(PlayerPreLoginEvent.@NotNull Result result) {}

    public static HandlerList getHandlerList() {
        return AsyncPlayerPreLoginEvent.getHandlerList();
    }

}
