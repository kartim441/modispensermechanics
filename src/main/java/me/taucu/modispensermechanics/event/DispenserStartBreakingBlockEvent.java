package me.taucu.modispensermechanics.event;

import org.bukkit.block.Block;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class DispenserStartBreakingBlockEvent extends BlockDispenseEvent {

    private final Block targetBlock;

    public DispenserStartBreakingBlockEvent(@NotNull Block block, @NotNull ItemStack tool, @NotNull Block targetBlock) {
        super(block, tool, new Vector());
        this.targetBlock = targetBlock;
    }

    public Block getTargetBlock() {
        return targetBlock;
    }

    public static HandlerList getHandlerList() {
        return BlockDispenseEvent.getHandlerList();
    }

}
