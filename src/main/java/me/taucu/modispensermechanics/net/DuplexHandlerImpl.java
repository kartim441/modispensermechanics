package me.taucu.modispensermechanics.net;

import com.mojang.brigadier.suggestion.Suggestions;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.util.DuplexHandler;
import me.taucu.modispensermechanics.util.NetworkUtil;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayerManager;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundCommandSuggestionsPacket;
import net.minecraft.network.protocol.game.ClientboundPlayerInfoRemovePacket;
import net.minecraft.network.protocol.game.ClientboundPlayerInfoUpdatePacket;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class DuplexHandlerImpl extends DuplexHandler {

    public static final String NAME = "me.taucu.modispensermechanics:handler";
    private static final Field FIELD_INFOUPDATEPACKET_ENTRIES = getField_InfoUpdatePacket_Entries();

    private Connection connection;

    public DuplexHandlerImpl() {
        super(NAME);
    }

    @Override
    public void attach(Channel channel) {
        super.attach(channel);
        this.connection = NetworkUtil.getServerConnectionOrThrow(((InetSocketAddress) channel.remoteAddress()).getAddress());
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        if (msg instanceof ClientboundPlayerInfoUpdatePacket packet
                && packet.entries().stream().anyMatch(e -> FakePlayerManager.FAKE_PROFILE.equals(e.profile()))) {
            if (FIELD_INFOUPDATEPACKET_ENTRIES != null) {
                var entries = new ArrayList<>(packet.entries().size());
                for (var entry : packet.entries()) {
                    if (!FakePlayerManager.FAKE_PROFILE.equals(entry.profile())) {
                        entries.add(entry);
                    }
                }
                FIELD_INFOUPDATEPACKET_ENTRIES.set(packet, entries);
            } else {
                super.write(ctx, packet, promise);
                connection.send(new ClientboundPlayerInfoRemovePacket(List.of(FakePlayerManager.FAKE_PROFILE.getId())));
                return;
            }
        } else if (msg instanceof ClientboundCommandSuggestionsPacket packet) {
            if (packet.suggestions().stream().anyMatch(s -> FakePlayerManager.FAKE_PROFILE.getName().equalsIgnoreCase(s.text()))) {
                // can't remove from suggestions, need to redefine packet
                var suggestions = packet.toSuggestions();
                var newList = new ArrayList<>(suggestions.getList());
                newList.removeIf(s -> FakePlayerManager.FAKE_PROFILE.getName().equalsIgnoreCase(s.getText()));
                suggestions = new Suggestions(suggestions.getRange(), newList);
                msg = new ClientboundCommandSuggestionsPacket(packet.id(), suggestions);
            }
        }
        super.write(ctx, msg, promise);
    }

    public static Field getField_InfoUpdatePacket_Entries() {
        try {
            for (Field f : ClientboundPlayerInfoUpdatePacket.class.getDeclaredFields()) {
                if (f.getType().isAssignableFrom(List.class) && f.getGenericType() instanceof ParameterizedType type) {
                    Type[] types = type.getActualTypeArguments();
                    if (types.length == 1 && ((Class<?>) types[0]).isAssignableFrom(ClientboundPlayerInfoUpdatePacket.Entry.class)) {
                        f.setAccessible(true);
                        return f;
                    }
                }
            }
        } catch (Throwable t) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Failed to find ClientboundPlayerInfoUpdatePacket.entries field");
        }
        return null;
    }

}
