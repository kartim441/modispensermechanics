package me.taucu.modispensermechanics.util;

import java.util.Collection;
import java.util.function.Function;

public class CollectionUtil {

    public static <T, R, O extends Collection<R>> O map(Collection<T> input, O output, Function<T, R> mapper) {
        for (T t : input) output.add(mapper.apply(t));
        return output;
    }

}
