package me.taucu.modispensermechanics.util;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.PluginClassLoader;

import java.lang.reflect.Field;

public class SpaghetUtil {

    public static Class<?> whoTouchedSpaghet(int startFrames, int endFrames, boolean skipBukkit) {
        return StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk(s -> s
                        .map(StackWalker.StackFrame::getDeclaringClass)
                        .dropWhile(declaringClass -> skipBukkit && declaringClass.getClassLoader() == Bukkit.getServer().getClass().getClassLoader())
                        .dropWhile(declaringClass -> declaringClass.getClassLoader() == Field.class.getClassLoader())
                        .limit(endFrames)
                        .skip(startFrames + 1)
                        .filter(declaringClass -> declaringClass.getClassLoader() instanceof PluginClassLoader pcl && pcl != MoDispenserMechanics.class.getClassLoader())
                        .findFirst())
                .orElse(null);
    }

}
