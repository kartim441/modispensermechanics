package me.taucu.modispensermechanics.util.fakeplayer;

import com.mojang.authlib.GameProfile;
import com.mojang.datafixers.util.Either;
import me.taucu.modispensermechanics.util.BukkitUtil;
import me.taucu.modispensermechanics.util.ReflectUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ClientInformation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.network.CommonListenerCookie;
import net.minecraft.stats.Stat;
import net.minecraft.util.Unit;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.player.ChatVisiblity;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.Level;

import java.lang.reflect.Field;

public class FakePlayer extends ServerPlayer {

    public static boolean allowFakePlayerEvents = false;

    public final MyServerPlayerGameMode myGameMode;

    private boolean secondaryUseActive = false;

    public FakePlayer(Level level, Connection connection, GameProfile profile) {
        super(level.getServer(), (ServerLevel) level, profile, new ClientInformation("", 0, ChatVisiblity.FULL, true, 0, HumanoidArm.RIGHT, false, false));
        CommonListenerCookie cookie = CommonListenerCookie.createInitial(profile, false);
        this.connection = BukkitUtil.IS_PAPER ? new FakeServerGamePacketListenerPaper(level.getServer(), connection, this, cookie) : new FakeServerGamePacketListener(level.getServer(), connection, this, cookie);
        if (BukkitUtil.IS_PAPER) {
            this.isRealPlayer = false;
            this.affectsSpawning = false;
            setLoadViewDistance(2);
            setSendViewDistance(2);
            setTickViewDistance(2);
        }
        getAbilities().mayBuild = true;
        getAbilities().invulnerable = true;
        getAbilities().mayfly = true;
        this.myGameMode = new MyServerPlayerGameMode(this);
        Field gameModeField = ReflectUtil.resolveDeclaredFieldByEquality(ServerPlayer.class, this, this.gameMode);
        try {
            gameModeField.set(this, myGameMode);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }

        getBukkitEntity().readExtraData(new CompoundTag());
        postDispense();
    }

    public void postDispense() {
        // clear inv
        Inventory playerInv = getInventory();
        playerInv.items.clear();
        playerInv.armor.clear();
        playerInv.offhand.clear();

        // reset secondary use
        setSecondaryUseActive(false);
        // stop using item
        if (!getUseItem().isEmpty() && isUsingItem()) {
            stopUsingItem();
        }

        // make invisible
        if (!this.activeEffects.containsKey(MobEffects.INVISIBILITY)) {
            this.activeEffects.put(MobEffects.INVISIBILITY, new MobEffectInstance(MobEffects.INVISIBILITY, MobEffectInstance.INFINITE_DURATION, 0, false, false, true));
        }

        // prevent being counted for sleep
        setGameType(GameType.SPECTATOR);
    }

    public void reposition() {
        ServerLevel level = serverLevel();
        if (level == null) return;

        BlockPos pos = level.levelData.getSpawnPos();

        if (getX() != pos.getX() || getY() != Short.MAX_VALUE || getZ() != pos.getZ()) {
            setPos(pos.getX(), Short.MAX_VALUE, pos.getZ());
        }
    }

    public void addToLevelWithPosition(ServerLevel level, double x, double y, double z) {
        if (level == null || isRemoved() || serverLevel() != level) {
            if (!isRemoved()) {
                setRemoved(RemovalReason.CHANGED_DIMENSION);
            }
            setLevel(level);
            gameMode.setLevel(level);
            setPos(x, y, z);
            if (level != null) {
                unsetRemoved();
                level.addNewPlayer(this);
            }
        } else {
            setPos(x, y, z);
        }
    }

    public MyServerPlayerGameMode getMyGameMode() {
        return myGameMode;
    }

    public void setGameType(GameType gameType) {
        getMyGameMode().setGameModeForPlayer(gameType);
    }

    public void setSecondaryUseActive(boolean active) {
        secondaryUseActive = active;
    }

    @Override
    public boolean isSecondaryUseActive() {
        return secondaryUseActive;
    }

    @Override
    public boolean isShiftKeyDown() {
        return secondaryUseActive;
    }

    public void finishUsingItem() {
        if (!useItem.isEmpty()) {
            completeUsingItem();
        }
    }

    @Override public void tick() {}
    @Override public void die(DamageSource source) {}
    @Override public void displayClientMessage(Component message, boolean actionBar) {}
    @Override public void sendSystemMessage(Component message) {}
    @Override public void awardStat(Stat stat, int amount) {}
    @Override public void setHealth(float health) {}

    @Override
    public boolean allowsListing() {
        return true;
    }

    // nothing should be allowed to hurt us
    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        return true;
    }

    // nothing should be allowed to hurt us
    @Override
    public boolean isAttackable() {
        return false;
    }

    // nothing should be allowed to hurt us
    @Override
    public boolean canHarmPlayer(Player player) {
        return false;
    }

    // prevent bad omen etc. (shouldn't matter, but we'll do it anyway)
    @Override
    public boolean canBeAffected(MobEffectInstance effect) {
        return false;
    }

    // no sleepy dispensers please
    @Override
    public Either<BedSleepingProblem, Unit> startSleepInBed(BlockPos blockposition, boolean force) {
        return Either.left(BedSleepingProblem.OTHER_PROBLEM);
    }

    // prevent armor being dispensed
    @Override
    public boolean canTakeItem(ItemStack stack) {
        return false;
    }

}
