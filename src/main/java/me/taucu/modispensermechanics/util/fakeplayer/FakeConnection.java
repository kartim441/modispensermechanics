package me.taucu.modispensermechanics.util.fakeplayer;

import me.taucu.modispensermechanics.util.SpaghetUtil;
import net.minecraft.network.Connection;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.PacketFlow;

public class FakeConnection extends Connection {

    public FakeConnection(PacketFlow flow) {
        super(flow);
    }

    @Override
    public void disconnect(Component disconnectReason) {
        if (SpaghetUtil.whoTouchedSpaghet(1, 2, false) == null) {
            super.disconnect(disconnectReason);
        }
        // fornicate yourself you filthy spaghet toucher!
    }

    // give other plugins an alternative if they need to for some reason(tm)
    public void realDisconnect(Component disconnectReason) {
        super.disconnect(disconnectReason);
    }

}
