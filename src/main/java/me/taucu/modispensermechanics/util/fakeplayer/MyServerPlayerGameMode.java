package me.taucu.modispensermechanics.util.fakeplayer;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ServerPlayerGameMode;
import net.minecraft.world.level.GameType;

public class MyServerPlayerGameMode extends ServerPlayerGameMode {

    public MyServerPlayerGameMode(ServerPlayer player) {
        super(player);
    }

    public void setGameModeForPlayer(GameType type) {
        setGameModeForPlayer(type, getGameModeForPlayer());
    }

    @Override
    public void setGameModeForPlayer(GameType type, GameType previousType) {
        super.setGameModeForPlayer(type, previousType);
    }

}
