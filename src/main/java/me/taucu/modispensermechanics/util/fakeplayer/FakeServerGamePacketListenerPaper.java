package me.taucu.modispensermechanics.util.fakeplayer;

import net.minecraft.network.Connection;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.game.ServerboundContainerClosePacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.CommonListenerCookie;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.jetbrains.annotations.Nullable;

public class FakeServerGamePacketListenerPaper extends FakeServerGamePacketListener {

    public FakeServerGamePacketListenerPaper(MinecraftServer server, Connection connection, FakePlayer player, CommonListenerCookie cookie) {
        super(server, connection, player, cookie);
    }

    @Override public void onDisconnect(Component reason, @Nullable net.kyori.adventure.text.Component quitMessage) {}
    @Override public void disconnect(net.kyori.adventure.text.Component reason, PlayerKickEvent.Cause cause) {}
    @Override public void handleCommand(String s) {}
    @Override public void handleContainerClose(ServerboundContainerClosePacket packet, InventoryCloseEvent.Reason reason) {}

}
