package me.taucu.modispensermechanics.util.fakeplayer;

import com.google.common.base.Preconditions;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.event.internal.AsyncFakePlayerPreLoginEvent;
import me.taucu.modispensermechanics.event.internal.FakePlayerLoginEvent;
import me.taucu.modispensermechanics.util.BukkitUtil;
import me.taucu.modispensermechanics.util.SpaghetUtil;
import me.taucu.modispensermechanics.util.VoidMap;
import net.minecraft.SharedConstants;
import net.minecraft.network.Connection;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.PacketFlow;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.network.ServerConnectionListener;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemCooldowns;
import net.minecraft.world.item.Items;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

public class FakePlayerManager {

    // use a dash to prevent name conflicts. If plugins want to be nitty pikky about it, too bad because it's JOINING ANYWAY!
    public static final String FAKE_PROFILE_NAME = "Dispenser-Block";
    // use citizens/offline-player style "NPC:" + name
    public static final GameProfile FAKE_PROFILE = generateProfile();
    private static GameProfile generateProfile() {
        GameProfile gp = new GameProfile(
                UUID.nameUUIDFromBytes(("MoDispenserMechanics:" + FAKE_PROFILE_NAME).getBytes(StandardCharsets.UTF_8)),
                FAKE_PROFILE_NAME
        );
        // fix skull texture lookups
        gp.getProperties().put("textures", new Property("textures", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2M1ZjNjM2Q1ZDdiMWNlMGYzMjA0MGMxNDU0ZGFmODg3OGUxY2MyOTllMDI1YmFkMmU2OTE5ZWJhZTIxZmJiOCJ9fX0="));
        return gp;
    }

    private static final Field FIELD_PLAYERLIST_PLAYERS_BY_UUID = getField_PlayerList_PlayersByUUID();
    private static final Field FIELD_PLAYERLIST_PLAYERS_BY_NAME = getField_PlayerList_PlayersByName();

    private static InetSocketAddress localhost = null;
    private static ServerSocket remoteSocket = null;
    private static InetSocketAddress remoteSocketAddress = null;
    private static FakeConnection fakeConnection = null;
    private static FakePlayer fakePlayer = null;
    private static WeakReference<Thread> loginThread = new WeakReference<>(null);
    private static CompletableFuture<AsyncFakePlayerPreLoginEvent> loginFuture;
    private static boolean wasAccessed = false;

    public static void preInit() {
        if (fakeConnection != null) throw new IllegalStateException("already pre-initialized");
        try {
            localhost = new InetSocketAddress(InetAddress.getLocalHost(), Bukkit.getPort());
            remoteSocket = new ServerSocket(0, 1, InetAddress.getLoopbackAddress());
            remoteSocketAddress = ((InetSocketAddress) remoteSocket.getLocalSocketAddress());
        } catch (IOException | ClassCastException e) {
            throw new RuntimeException("Error initializing connections:", e);
        }

        // otherwise some code will try to look up our profile blocking the main thread
        MinecraftServer.getServer().getProfileCache().add(FAKE_PROFILE);

        fakeConnection = createConnection();

        // prevent NoClassDefFoundError if the jar has changed (as SpaghetUtil is not always loaded)
        Class<?> ignored = SpaghetUtil.class;
    }

    @SuppressWarnings({"deprecation", "unchecked"})
    public static void init() {
        if (fakeConnection == null) throw new IllegalStateException("preInit must be called before init");
        if (fakePlayer != null) throw new IllegalStateException("already initialized");
        loginFuture = CompletableFuture.supplyAsync(() -> {
            loginThread = new WeakReference<>(Thread.currentThread());
            AsyncFakePlayerPreLoginEvent playerPreLoginEvent = new AsyncFakePlayerPreLoginEvent(FAKE_PROFILE_NAME, localhost.getAddress(), FAKE_PROFILE.getId());
            Bukkit.getPluginManager().callEvent(playerPreLoginEvent);
            return playerPreLoginEvent;
        }, fakeConnection.channel.eventLoop());

        fakePlayer = createFakePlayer(FAKE_PROFILE);
        fakePlayer.getBukkitEntity().setMetadata("me.taucu.modispensermechanics.fakeplayer", new FixedMetadataValue(MoDispenserMechanics.getInstance(), true));
        fakePlayer.getBukkitEntity().setMetadata("NPC", new FixedMetadataValue(MoDispenserMechanics.getInstance(), true));

        AsyncFakePlayerPreLoginEvent fakePlayerPreLoginEvent = null;
        int time = 10;
        TimeUnit unit = TimeUnit.SECONDS;
        try {
            if (!loginFuture.isDone()) {
                MoDispenserMechanics.getInstance().getLogger().log(Level.WARNING, "Waiting for fake player login to finish.");
            }
            fakePlayerPreLoginEvent = loginFuture.get(time, unit);
        } catch (InterruptedException e) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Interrupted while performing fake player login!", e);
        } catch (TimeoutException e) {
            Exception err = e;
            Thread thread = loginThread.get();
            if (thread != null) {
                err = new TimeoutException("Thread dump:");
                err.setStackTrace(thread.getStackTrace());
            }
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Fake player login took longer than " + time + " " + unit.toString().toLowerCase() + " to complete", err);
        } catch (ExecutionException e) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while preforming fake player login", e);
        }

        PlayerList list = MinecraftServer.getServer().getPlayerList();
        list.players.add(fakePlayer);
        try {
            Map<UUID, ServerPlayer> playersByUUID = (Map<UUID, ServerPlayer>) FIELD_PLAYERLIST_PLAYERS_BY_UUID.get(list);
            Map<String, ServerPlayer> playersByName = (Map<String, ServerPlayer>) FIELD_PLAYERLIST_PLAYERS_BY_NAME.get(list);
            playersByUUID.put(fakePlayer.getUUID(), fakePlayer);
            playersByName.put(fakePlayer.getScoreboardName().toLowerCase(java.util.Locale.ROOT), fakePlayer);
        } catch (Throwable t) {
            shutdown();
            throw new RuntimeException(t);
        }

        fakePlayer.serverLevel().addNewPlayer(fakePlayer);

        if (fakePlayerPreLoginEvent != null) {
            Bukkit.getPluginManager().callEvent(new FakePlayerLoginEvent(fakePlayer.getBukkitEntity(), "localhost:" + Bukkit.getPort(), fakePlayerPreLoginEvent.getAddress()));
            Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(fakePlayer.getBukkitEntity(), ""));

            // after join some plugins may have hidden us.
            Bukkit.getOnlinePlayers().forEach(p -> p.showPlayer(fakePlayer.getBukkitEntity()));
        }

        fakePlayer.getInventory().clearContent();
        fakePlayer.getAbilities().invulnerable = true;

        fakePlayer.postDispense();
    }

    @SuppressWarnings({"deprecation", "unchecked"})
    public static void shutdown() {
        FakePlayer fp = fakePlayer;
        if (fp != null) {
            fakePlayer = null;
            if (loginFuture.getNow(null) != null) {
                Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(fp.getBukkitEntity(), "shutdown"));
            }
            fp.setRemoved(Entity.RemovalReason.DISCARDED);
            PlayerList list = MinecraftServer.getServer().getPlayerList();
            list.players.remove(fp);
            try {
                Map<UUID, ServerPlayer> playersByUUID = (Map<UUID, ServerPlayer>) FIELD_PLAYERLIST_PLAYERS_BY_UUID.get(list);
                Map<String, ServerPlayer> playersByName = (Map<String, ServerPlayer>) FIELD_PLAYERLIST_PLAYERS_BY_NAME.get(list);
                playersByUUID.remove(fp.getUUID(), fp);
                playersByName.remove(fp.getScoreboardName().toLowerCase(java.util.Locale.ROOT), fp);
            } catch (Throwable t) {
                MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Error while removing fake player from PlayerList", t);
            }
        }
        if (fakeConnection != null) {
            try {
                fakeConnection.disconnect(Component.literal("shutdown"));
            } finally {
                fakeConnection = null;
            }
        }
        if (remoteSocket != null) {
            try {
                remoteSocket.close();
            } catch (IOException ignored) {}
            remoteSocket = null;
            remoteSocketAddress = null;
        }
    }

    public static FakePlayer createFakePlayer(GameProfile profile) {
        Preconditions.checkNotNull(fakeConnection, "fakeConnection is null");
        FakePlayer fp = new FakePlayer(MinecraftServer.getServer().getLevel(ServerLevel.OVERWORLD), fakeConnection, FAKE_PROFILE);

        // otherwise we get advancements
        fp.getAdvancements().stopListening();

        // prevent item cooldowns
        fp.getCooldowns().addCooldown(Items.STONE, 1095473923);
        boolean logged = false;
        Class<?> check = fp.getCooldowns().getClass();
        do {
            for (Field f : check.getDeclaredFields()) {
                if (f.getType().isAssignableFrom(HashMap.class)) {
                    try {
                        f.setAccessible(true);
                        HashMap<?, ?> map = (HashMap<?, ?>) f.get(fp.getCooldowns());
                        if (map != null && !map.isEmpty() && map.get(Items.STONE) instanceof ItemCooldowns.CooldownInstance inst && inst.endTime == 1095473923) {
                            f.set(fp.getCooldowns(), new VoidMap<Item, ItemCooldowns.CooldownInstance>());
                        }
                    } catch (IllegalAccessException e) {
                        MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Couldn't modify item cooldowns map: " + f.getName(), e);
                        logged = true;
                    }
                }
            }
        } while ((check = check.getSuperclass()) != null && check.getName().startsWith("net.minecraft"));

        if (!logged && !(fp.getCooldowns().cooldowns instanceof VoidMap<Item, ItemCooldowns.CooldownInstance>)) {
            MoDispenserMechanics.getInstance().getLogger().log(Level.SEVERE, "Failed to find item cooldowns map");
        }

        return fp;
    }

    private static FakeConnection createConnection() {
        FakeConnection fakeConnection = new FakeConnection(PacketFlow.SERVERBOUND);
        FakeChannel fc = new FakeChannel(localhost, remoteSocketAddress);
        Connection.configureSerialization(fc.pipeline(), PacketFlow.SERVERBOUND, false, null);
        fc.pipeline().addLast("packet_handler", fakeConnection);
        getEventLoop().register(fc);

        fakeConnection.channel = fc;

        fakeConnection.address = remoteSocketAddress;
        if (BukkitUtil.IS_PAPER) {
            fakeConnection.virtualHost = remoteSocketAddress;
            fakeConnection.protocolVersion = SharedConstants.getCurrentVersion().getProtocolVersion();
        }

        return fakeConnection;
    }

    private static Field getField_PlayerList_PlayersByUUID() {
        try {
            for (Field f : PlayerList.class.getDeclaredFields()) {
                if (f.getType().isAssignableFrom(Map.class) && f.getGenericType() instanceof ParameterizedType type) {
                    Type[] types = type.getActualTypeArguments();
                    if (types.length == 2 && ((Class<?>) types[0]).isAssignableFrom(UUID.class) && ((Class<?>) types[1]).isAssignableFrom(ServerPlayer.class)) {
                        f.setAccessible(true);
                        return f;
                    }
                }
            }
            throw new NoSuchFieldException("Could not find PlayerList.playersByUUID field");
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private static Field getField_PlayerList_PlayersByName() {
        try {
            Field f = PlayerList.class.getDeclaredField("playersByName");
            f.setAccessible(true);
            return f;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    private static EventLoopGroup getEventLoop() {
        if (Epoll.isAvailable() && MinecraftServer.getServer().isEpollEnabled()) {
            return ServerConnectionListener.SERVER_EPOLL_EVENT_GROUP.get().next();
        } else {
            return ServerConnectionListener.SERVER_EVENT_GROUP.get().next();
        }
    }

    public static FakePlayer getFakePlayer() {
        return getFakePlayer(fakePlayer.serverLevel());
    }

    public static FakePlayer getFakePlayer(ServerLevel level) {
        return getFakePlayer(level, 0, Short.MAX_VALUE, 0);
    }

    public static FakePlayer getFakePlayer(ServerLevel level, double x, double y, double z) {
        wasAccessed = true;
        fakePlayer.addToLevelWithPosition(level, x, y, z);
        return fakePlayer;
    }

    public static void postHandle() {
        if (fakePlayer != null && wasAccessed) {
            wasAccessed = false;
            fakePlayer.reposition();
        }
    }

}
