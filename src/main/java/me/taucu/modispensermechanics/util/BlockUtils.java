package me.taucu.modispensermechanics.util;

import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class BlockUtils {

    public static float getDestroyRate(Level level, BlockPos pos, BlockState state, ItemStack stack) {
        // get the destroySpeed of the tool used versus the block it is being used on
        // modeled off of Player#getDestroySpeed
        float destroySpeed = stack.getDestroySpeed(state);
        if (destroySpeed > 1.0F) {
            int efficiency = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.EFFICIENCY, stack);
            if (efficiency > 0) {
                destroySpeed += (float) (efficiency * efficiency + 1);
            }
        }

        // modeled off of BlockBehavior#getDestroyProgress
        float hardness = state.getDestroySpeed(level, pos);
        if (hardness == -1.0F) { // unbreakable
            return 0.0F;
        } else if (hardness == 0) { // instamine
            return 1.0F;
        } else {
            return destroySpeed / hardness / (!state.requiresCorrectToolForDrops() || stack.isCorrectToolForDrops(state) ? 30 : 100);
        }
    }

}
