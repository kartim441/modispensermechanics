package me.taucu.modispensermechanics.util;

import net.minecraft.core.Holder;
import net.minecraft.core.component.DataComponents;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import net.minecraft.world.item.enchantment.DigDurabilityEnchantment;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.block.state.BlockState;

public class ItemStackUtils {

    public static boolean hurtAndBreak(int amount, ItemStack stack, RandomSource random) {
        if (hurt(amount, stack, random)) {
            stack.shrink(1);
            stack.setDamageValue(0);
            return true;
        }
        return false;
    }

    // modeled off of ItemStack#hurt
    public static boolean hurt(int amount, ItemStack stack, RandomSource random) {
        if (stack.isDamageableItem()) {

            if (amount > 0) {
                int unbreaking = EnchantmentHelper.getItemEnchantmentLevel(Enchantments.UNBREAKING, stack);
                for (int i = 0; unbreaking > 0 && i < amount; i++) {
                    if (DigDurabilityEnchantment.shouldIgnoreDurabilityDrop(stack, unbreaking, random)) {
                        amount--;
                    }
                }

                if (amount <= 0) {
                    return false;
                }
            }

            int newDamage = stack.getDamageValue() + amount;
            stack.setDamageValue(newDamage);
            return newDamage >= stack.getMaxDamage();
        } else {
            return false;
        }
    }

    // modeled off of AttributeInstance#calculateValue and ItemStack#forEachModifier
    public static double calculateAttribute(ItemStack stack, EquipmentSlot slot, Holder<Attribute> holder, double base) {
        ItemAttributeModifiers itemModifiers = stack.get(DataComponents.ATTRIBUTE_MODIFIERS);
        if (itemModifiers == null)
            return base;

        double value = base;

        for (var entry : itemModifiers.modifiers()) {
            if (entry.slot().test(slot) && entry.attribute().value().equals(holder.value())) {
                var mod = entry.modifier();
                double amt = mod.amount();

                value += switch (mod.operation()) {
                    case ADD_VALUE -> amt;
                    case ADD_MULTIPLIED_BASE -> amt * base;
                    case ADD_MULTIPLIED_TOTAL -> amt * value;
                };
            }
        }

        return holder.value().sanitizeValue(value);
    }

    public static boolean couldToolBreak(ItemStack tool, BlockState state) {
        return couldToolBreak(tool, tool.isCorrectToolForDrops(state) ? 1 : 2);
    }

    public static boolean couldToolBreak(ItemStack tool, int damage) {
        return tool.isDamageableItem() && tool.getDamageValue() + damage >= tool.getMaxDamage();
    }

    // modeled off of Player#attack
    public static float getAttackDamageScale(long currentGameTime, long lastUsedGameTime, ItemStack stack, float baseTime) {
        double attackSpeed = calculateAttribute(stack, EquipmentSlot.MAINHAND, Attributes.ATTACK_SPEED, Attributes.ATTACK_SPEED.value().getDefaultValue());
        float attackStrengthDelay = (float) (1.0D / attackSpeed * 20.0D);
        return Mth.clamp((Math.max(0, currentGameTime - lastUsedGameTime) + baseTime) / attackStrengthDelay, 0.0F, 1.0F);
    }

}
