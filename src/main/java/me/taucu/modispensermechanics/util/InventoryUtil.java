package me.taucu.modispensermechanics.util;

import net.minecraft.core.Direction;
import net.minecraft.world.Container;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.ChestType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class InventoryUtil {

    public static boolean isEmpty(List<ItemStack> stacks) {
        for (ItemStack stack : stacks) {
            if (!stack.isEmpty()) return false;
        }
        return true;
    }

    public static int nextFreeSlot(List<ItemStack> inv) {
        for (int i = 0; i < inv.size(); i++) {
            ItemStack stack = inv.get(i);
            if (stack == null || stack.isEmpty()) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(List<ItemStack> stacks, ItemStack stack) {
        return indexOf(stacks, stack, false);
    }

    public static int indexOf(List<ItemStack> stacks, ItemStack stack, boolean itemOnly) {
        Item item = stack.getItem();
        for (int slot = 0; slot < stacks.size(); slot++) {
            ItemStack test = stacks.get(slot);
            if (test.is(item) && (itemOnly || ItemStack.matches(test, stack))) {
                return slot;
            }
        }
        return -1;
    }

    public static Container[] getConnectedContainers(Container cont) {
        if (cont instanceof ChestBlockEntity chestEntity) {
            BlockState chestState = chestEntity.getBlockState();
            if (chestState.getBlock() instanceof ChestBlock) {
                ChestType chestType = chestState.getValue(ChestBlock.TYPE);
                Container right, left;
                if (chestEntity.hasLevel() &&
                        chestType != ChestType.SINGLE &&
                        chestEntity.getLevel().getBlockEntity(chestEntity.getBlockPos().relative(ChestBlock.getConnectedDirection(chestState))) instanceof ChestBlockEntity connected) {
                    if (chestType == ChestType.RIGHT) {
                        right = chestEntity;
                        left = connected;
                    } else {
                        right = connected;
                        left = chestEntity;
                    }
                    return new Container[] {right, left};
                }
            }
        }
        return new Container[] {cont};
    }

    public static int[] itemCounts(Container cont) {
        int[] counts = new int[cont.getContainerSize()];
        for (int i = 0; i < counts.length; i++) {
            ItemStack stack = cont.getItem(i);
            counts[i] = stack.isEmpty() ? 0 : stack.getCount();
        }
        return counts;
    }

    public static int[] itemCounts(List<ItemStack> stacks) {
        int[] counts = new int[stacks.size()];
        for (int i = 0; i < stacks.size(); i++) {
            ItemStack stack = stacks.get(i);
            counts[i] = stack.isEmpty() ? 0 : stack.getCount();
        }
        return counts;
    }

    public static boolean canMergeItems(Container to, List<ItemStack> toMerge, @Nullable Direction face) {
        return hasRemaining(remainingCountsFromMerge(to, toMerge, face));
    }

    public static boolean hasRemaining(int[] remainingCounts) {
        for (int count : remainingCounts) {
            if (count > 0) return false;
        }
        return true;
    }

    public static int[] remainingCountsFromMerge(Container to, List<ItemStack> toMerge, @Nullable Direction face) {
        int[] toMergeCounts = itemCounts(toMerge);
        for (Container cont : getConnectedContainers(to)) {
            if (face != null && cont instanceof WorldlyContainer worldly) {
                remainingCountsFromMerge(cont.getContents(), toMerge, toMergeCounts, cont.getMaxStackSize(), worldly, face);
            } else {
                remainingCountsFromMerge(cont.getContents(), toMerge, toMergeCounts, cont.getMaxStackSize());
            }
        }
        return toMergeCounts;
    }

    public static int[] remainingCountsFromMerge(List<ItemStack> to, List<ItemStack> toMergeStacks, int[] toMergeCounts, int containerMaxStackSize) {
        for (int i = 0; i < to.size(); i++) {
            remainingCountsFromMergeWithSlot(to, i, toMergeStacks, toMergeCounts, containerMaxStackSize, null, null);
        }
        return toMergeCounts;
    }

    public static int[] remainingCountsFromMerge(List<ItemStack> to, List<ItemStack> toMergeStacks, int[] toMergeCounts, int containerMaxStackSize, @NotNull WorldlyContainer worldly, @NotNull Direction face) {
        for (int i : worldly.getSlotsForFace(face)) {
            remainingCountsFromMergeWithSlot(to, i, toMergeStacks, toMergeCounts, containerMaxStackSize, worldly, face);
        }
        return toMergeCounts;
    }

    public static void remainingCountsFromMergeWithSlot(List<ItemStack> to, int pos, List<ItemStack> toMergeStacks, int[] toMergeCounts, int containerMaxStackSize, @Nullable WorldlyContainer worldly, @Nullable Direction face) {
        ItemStack toStack = to.get(pos);
        int remainingToCount = Math.min(toStack.getMaxStackSize(), containerMaxStackSize) - toStack.getCount();
        for (int i = 0; i < toMergeStacks.size(); i++) {
            ItemStack toMergeStack = toMergeStacks.get(i);
            int diff = Math.min(toMergeCounts[i], remainingToCount);
            if (diff > 0 && (worldly == null || worldly.canPlaceItemThroughFace(pos, toMergeStack, face))) {
                if (toStack == null || toStack.isEmpty()) {
                    toStack = toMergeStack;
                    remainingToCount = Math.min(toStack.getMaxStackSize(), containerMaxStackSize) - toStack.getCount();
                    toMergeCounts[i] -= diff;
                } else {
                    if (toStack.is(toMergeStack.getItem()) && Objects.equals(toStack.getComponents(), toMergeStack.getComponents())) {
                        remainingToCount -= diff;
                        toMergeCounts[i] -= diff;
                    }
                }
            }
        }
    }

    public static <L extends List<ItemStack>> L mergeItems(L toMerge, int containerMaxStackSize) {
        mergeItems(toMerge, toMerge, containerMaxStackSize);
        return toMerge;
    }

    public static Container[] mergeItems(Container to, List<ItemStack> toMerge, @Nullable Direction face) {
        Container[] connected = getConnectedContainers(to);
        for (Container cont : connected) {
            if (face != null && cont instanceof WorldlyContainer worldly) {
                if (mergeItems(cont.getContents(), toMerge, cont.getMaxStackSize(), worldly, face)) break;
            } else {
                if (mergeItems(cont.getContents(), toMerge, cont.getMaxStackSize())) break;
            }
        }
        return connected;
    }

    public static boolean mergeItems(List<ItemStack> to, List<ItemStack> toMerge, int containerMaxStackSize) {
        for (int i = 0; i < to.size(); i++) {
            if (mergeItemsWithSlot(to, i, toMerge, containerMaxStackSize)) return true;
        }
        return false;
    }

    public static boolean mergeItems(List<ItemStack> to, List<ItemStack> toMerge, int containerMaxStackSize, @NotNull WorldlyContainer worldly, @NotNull Direction face) {
        for (int i : worldly.getSlotsForFace(face)) {
            if (mergeItemsWithSlot(to, i, toMerge, containerMaxStackSize)) return true;
        }
        return false;
    }

    public static boolean mergeItemsWithSlot(List<ItemStack> to, int pos, List<ItemStack> toMerge, int containerMaxStackSize) {
        boolean empty = true;
        for (int i = 0; i < toMerge.size(); i++) {
            ItemStack toMergeStack = toMerge.get(i);
            if (toMergeStack != null && !toMergeStack.isEmpty()) {
                ItemStack toStack = to.get(pos);
                if (toStack == toMergeStack) {
                    to.set(pos, toMerge.set(i, toMergeStack = ItemStack.EMPTY));
                } else if (toStack == null || toStack.isEmpty()) {
                    to.set(pos, toMergeStack.split(Math.min(toMergeStack.getCount(), containerMaxStackSize)));
                } else if (toStack.is(toMergeStack.getItem()) && toStack.getCount() < toStack.getMaxStackSize() && Objects.equals(toStack.getComponents(), toMergeStack.getComponents())) {
                    int diff = Math.min(toMergeStack.getCount(), Math.min(toStack.getMaxStackSize(), containerMaxStackSize) - toStack.getCount());
                    toMergeStack.shrink(diff);
                    toStack.grow(diff);
                }
                empty &= toMergeStack.isEmpty();
            }
        }
        return empty;
    }

    public static ItemStack mergeItemsWithItem(ItemStack toStack, List<ItemStack> toMerge, int itemMaxStackSize) {
        List<ItemStack> to = Arrays.asList(toStack);
        mergeItemsWithSlot(to, 0, toMerge, itemMaxStackSize);
        return to.getFirst();
    }


}
