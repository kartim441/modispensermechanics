package me.taucu.modispensermechanics.util;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventUtil {

    public static void enforcePriority(Listener listener, JavaPlugin plugin) {
        List<Class<? extends Event>> events = new ArrayList<>();
        Class<?> clazz = listener.getClass();
        do {
            for (Method m : clazz.getDeclaredMethods()) {
                if (m.getAnnotation(EventHandler.class) != null && m.getParameterCount() == 1 && Arrays.stream(m.getParameters()).anyMatch(p -> Event.class.isAssignableFrom(p.getType()))) {
                    enforcePriority(listener, m.getParameters()[0].getType(), plugin);
                }
            }
        } while ((clazz = clazz.getSuperclass()) != null && Listener.class.isAssignableFrom(clazz));
    }

    public static void enforcePriority(Listener listener, Class<?> eventClazz, JavaPlugin plugin) {
        HandlerList lst = getHandlerList(eventClazz);
        RegisteredListener[] listeners = lst.getRegisteredListeners();

        for (RegisteredListener rl : listeners) {
            lst.unregister(rl);
        }

        try {
            Arrays.sort(listeners, (a, b) -> {
                return Float.compare(
                        a.getPriority().getSlot() + (a.getListener() == listener ? (a.getPriority().getSlot() > 0 ? 0.5F : -0.5F) : 0),
                        b.getPriority().getSlot() + (b.getListener() == listener ? (b.getPriority().getSlot() > 0 ? 0.5F : -0.5F) : 0)
                );
            });
            for (RegisteredListener rl : listeners) {
                lst.register(rl);
            }
            lst.bake();
        } catch (Throwable t) {
            if (t instanceof ThreadDeath) {
                throw t;
            }
            for (RegisteredListener rl : listeners) {
                lst.unregister(rl);
                lst.register(rl);
            }
        }
    }

    public static HandlerList getHandlerList(Class<?> eventClazz) {
        try {
            return (HandlerList) eventClazz.getMethod("getHandlerList").invoke(null);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

}
