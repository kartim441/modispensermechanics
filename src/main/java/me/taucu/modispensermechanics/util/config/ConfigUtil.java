package me.taucu.modispensermechanics.util.config;

import com.mojang.datafixers.util.Pair;
import me.taucu.modispensermechanics.MoDispenserMechanics;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderSet;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;

import java.util.*;
import java.util.function.Function;

public class ConfigUtil {

    public static <T> T resolve(String key, Registry<T> registry, String purpose) {
        ResourceLocation loc = ResourceLocation.tryParse(key);
        if (loc != null) {
            if (registry.containsKey(loc)) {
                return registry.get(loc);
            } else {
                MoDispenserMechanics.getInstance().getLogger().warning("Could not resolve: " + purpose + ": \"" + key + "\"");
            }
        } else {
            MoDispenserMechanics.getInstance().getLogger().warning("Could not parse: " + purpose + ": \"" + key + "\"");
        }
        return null;
    }

    public static <T> HashSet<T> resolve(List<String> keys, Registry<T> registry, String purpose) {
        HashSet<T> items = new HashSet<>();
        keys = splitKeys(keys, ",", String::trim);
        for (String key : keys) {
            resolve(key, registry, purpose, items);
        }
        return items;
    }

    public static <T> HashSet<T> resolve(String key, Registry<T> registry, String purpose, HashSet<T> output) {
        ResourceLocation loc;
        if (key.startsWith("#")) {
            loc = ResourceLocation.tryParse(key.substring(1));
            Optional<Pair<TagKey<T>, HolderSet.Named<T>>> opt = registry.getTags()
                    .filter(p -> p.getFirst().location().equals(loc))
                    .findAny();
            if (opt.isPresent()) {
                opt.get().getSecond().stream()
                        .map(Holder::value)
                        .filter(Objects::nonNull)
                        .forEach(output::add);
            } else {
                MoDispenserMechanics.getInstance().getLogger().warning("Could not resolve tag: " + purpose + ": \"" + key + "\"");
            }
        } else {
            T t = resolve(key, registry, purpose);
            if (t != null) output.add(t);
        }
        return output;
    }

    public static List<String> splitKeys(List<String> keys, String delimiter, Function<String, String> onFind) {
        List<String> output = new ArrayList<>();
        for (String k : keys) {
            int idx;
            while ((idx = k.indexOf(delimiter)) != -1) {
                output.add(onFind.apply(k.substring(0, idx)));
                k = k.substring(idx + delimiter.length());
            }
            output.add(onFind.apply(k));
        }
        return output;
    }

}
