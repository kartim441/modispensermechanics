package me.taucu.modispensermechanics.util.config;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.dispense.DispenseContext;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.DispenseResult;
import me.taucu.modispensermechanics.util.DispenserUtil;
import me.taucu.modispensermechanics.util.ItemStackUtils;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public enum ToolDurabilityOption {

    BREAK,
    DROP,
    JAM,
    UNBREAKABLE;

    private static ToolDurabilityOption option = BREAK;

    public static ToolDurabilityOption get() {
        return option;
    }

    public static void set(ToolDurabilityOption option) {
        ToolDurabilityOption.option = option;
    }

    public static ToolDurabilityOption resolve(String s) {
        try {
            return valueOf(s);
        } catch (IllegalArgumentException ignored) {
            MoDispenserMechanics.getInstance().getLogger().warning("Invalid tool break action: \"" + s + "\"");
        }
        return BREAK;
    }

    public static boolean use(DispenseMechanic mechanic, DispenseContext ctx, int amt) {
        if (option == BREAK || option == UNBREAKABLE || !ItemStackUtils.couldToolBreak(ctx.stack(), amt)) {
            return true;
        } else {
            if (ToolDurabilityOption.get() == ToolDurabilityOption.DROP) {
                ItemStack stack = ctx.stack();
                DispenserUtil.dropItem(ctx.level(), ctx.pos(), ctx.facing(), stack.split(1));
                if (stack.getCount() > 0) stack.setDamageValue(0);
            } else {
                // jam
                ctx.result(DispenseResult.FAILURE);
            }
            return false;
        }
    }

    public static boolean use(DispenseMechanic mechanic, DispenseContext ctx, BlockState state) {
        if (option == BREAK || option == UNBREAKABLE || !ItemStackUtils.couldToolBreak(ctx.stack(), state)) {
            return true;
        } else {
            if (ToolDurabilityOption.get() == ToolDurabilityOption.DROP) {
                ItemStack stack = ctx.stack();
                DispenserUtil.dropItem(ctx.level(), ctx.pos(), ctx.facing(), stack.split(1));
                if (stack.getCount() > 0) stack.setDamageValue(0);
            } else {
                // jam
                ctx.result(DispenseResult.FAILURE);
            }
            return false;
        }
    }

    public static boolean canBreak(ItemStack stack, BlockState state) {
        return option == BREAK || option == UNBREAKABLE || !ItemStackUtils.couldToolBreak(stack, state);
    }

    public static boolean consumes() {
        return option == BREAK || option == DROP;
    }

}
