package me.taucu.modispensermechanics.listeners;

import me.taucu.modispensermechanics.MoDispenserMechanics;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayer;
import me.taucu.modispensermechanics.util.fakeplayer.FakePlayerManager;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BellRingEvent;
import org.bukkit.event.block.BlockFertilizeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.*;

public class EntityListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onEntityDamageByEntityLowest(EntityDamageByEntityEvent e) {
        handle(e, e.getDamager(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onEntityDamageByEntityMonitor(EntityDamageByEntityEvent e) {
        handle(e, e.getDamager(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerBucketFillLowest(PlayerBucketFillEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerBucketFillMonitor(PlayerBucketFillEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerBucketEmptyLowest(PlayerBucketEmptyEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerBucketEmptyMonitor(PlayerBucketEmptyEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onCauldronLevelChangeLowest(CauldronLevelChangeEvent e) {
        handle(e, e.getEntity(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onCauldronLevelChangeMonitor(CauldronLevelChangeEvent e) {
        handle(e, e.getEntity(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onBlockPlaceLowest(BlockPlaceEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onBlockPlaceMonitor(BlockPlaceEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onEntityChangeBlockLowest(EntityChangeBlockEvent e) {
        handle(e, e.getEntity(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onEntityChangeBlockMonitor(EntityChangeBlockEvent e) {
        handle(e, e.getEntity(), false);
    }
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onBlockFertilizeLowest(BlockFertilizeEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onBlockFertilizeMonitor(BlockFertilizeEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onBellRingLowest(BellRingEvent e) {
        handle(e, e.getEntity(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onBellRingMonitor(BellRingEvent e) {
        handle(e, e.getEntity(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onConsumeItemLowest(PlayerItemConsumeEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onConsumeItemMonitor(PlayerItemConsumeEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent e) {
        // some plugins may have hidden the fake player
        e.getPlayer().showPlayer(FakePlayerManager.getFakePlayer().getBukkitEntity());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(PlayerLoginEvent e) {
        MoDispenserMechanics.attachHandler(e.getPlayer(), e.getAddress());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onTeleportMonitor(PlayerTeleportEvent e) {
        if (isFake(e.getPlayer())) e.setCancelled(true);
    }

    // this event isn't cancellable. piss.
    private boolean egg_handling = false;
    private PlayerEggThrowEvent egg_event;
    private boolean egg_hatching;
    private EntityType egg_hatchType;
    private byte egg_numHatches;
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerEggThrow(PlayerEggThrowEvent e) {
        if (!FakePlayer.allowFakePlayerEvents && !egg_handling && isFake(e.getPlayer()) && !e.isAsynchronous()) {
            egg_event = e;
            egg_hatching = e.isHatching();
            egg_hatchType = e.getHatchingType();
            egg_numHatches = e.getNumHatches();
            egg_handling = true;
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerEggThrowMonitor(PlayerEggThrowEvent e) {
        if (egg_handling && !e.isAsynchronous()) {
            if (e == egg_event) {
                e.setHatching(egg_hatching);
                e.setHatchingType(egg_hatchType);
                e.setNumHatches(egg_numHatches);
            }
            egg_handling = false;
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onTargetLowest(EntityTargetEvent e) {
        // never allow targeting
        if (isFake(e.getTarget())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onTargetMonitor(EntityTargetEvent e) {
        // never allow targeting
        if (isFake(e.getTarget())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onGameModeChange(PlayerGameModeChangeEvent e) {
        // never allow game mode change to anything but survival
        if (isFake(e.getPlayer())) e.setCancelled(e.getNewGameMode() != GameMode.SURVIVAL);
    }

    public static void handle(Cancellable c, Entity ent, boolean state) {
        if (!FakePlayer.allowFakePlayerEvents && isFake(ent)) {
            c.setCancelled(state);
        }
    }

    public static boolean isFake(Entity ent) {
        return ent != null && ((CraftEntity) ent).getHandle() instanceof FakePlayer;
    }

}
