package me.taucu.modispensermechanics.listeners;

import io.papermc.paper.event.block.BlockPreDispenseEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class PaperBlockListener implements Listener {

    public static int currentDispenseSlot = -1;
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onDispense(BlockPreDispenseEvent e) {
        currentDispenseSlot = e.getSlot();
    }

}
