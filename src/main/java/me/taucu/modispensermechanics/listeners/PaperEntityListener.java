package me.taucu.modispensermechanics.listeners;

import io.papermc.paper.event.block.PlayerShearBlockEvent;
import io.papermc.paper.event.player.PlayerFlowerPotManipulateEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

public class PaperEntityListener extends EntityListener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerShearBlockLowest(PlayerShearBlockEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerShearBlockMonitor(PlayerShearBlockEvent e) {
        handle(e, e.getPlayer(), false);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = false)
    public void onPlayerFlowerPotManipulateLowest(PlayerFlowerPotManipulateEvent e) {
        handle(e, e.getPlayer(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
    public void onPlayerFlowerPotManipulateMonitor(PlayerFlowerPotManipulateEvent e) {
        handle(e, e.getPlayer(), false);
    }

}
