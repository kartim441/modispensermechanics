package me.taucu.modispensermechanics.listeners;

import me.taucu.modispensermechanics.dispense.mechanics.ToolBreakBlockMechanic;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldLoadEvent;

public class ChunkListener implements Listener {

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent e) {
        ToolBreakBlockMechanic.processChunk(e.getChunk());
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent e) {
        ToolBreakBlockMechanic.processChunkUnload(e.getChunk());
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent e) {
        ToolBreakBlockMechanic.processWorldLoad(e.getWorld());
    }

}
