package me.taucu.modispensermechanics;

import me.taucu.modispensermechanics.dispense.DispenseItemHandler;
import me.taucu.modispensermechanics.dispense.DispenseMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.*;
import me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic.AutoCraftMechanic;
import me.taucu.modispensermechanics.dispense.mechanics.autocraftmechanic.CachedRecipe;
import me.taucu.modispensermechanics.dispense.tasks.ToolBreakBlockTask;
import me.taucu.modispensermechanics.dispense.wrappers.DispenseBehaviorWrapper;
import me.taucu.modispensermechanics.dispense.wrappers.NoOpOptionalDispenseBehaviorWrapper;
import me.taucu.modispensermechanics.dispense.wrappers.OptionalDispenseBehaviorWrapper;
import me.taucu.modispensermechanics.event.RegisterMechanicsEvent;
import me.taucu.modispensermechanics.util.config.ConfigUtil;
import net.minecraft.core.dispenser.DefaultDispenseItemBehavior;
import net.minecraft.core.dispenser.DispenseItemBehavior;
import net.minecraft.core.dispenser.OptionalDispenseItemBehavior;
import net.minecraft.core.dispenser.ShearsDispenseItemBehavior;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.*;
import net.minecraft.world.level.block.DispenserBlock;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class CustomBehaviorManager {

    final HashMap<Item, DispenseItemHandler> currentHandlers = new HashMap<>();
    final HashMap<Item, DispenseItemBehavior> oldBehaviorsRevert = new HashMap<>();

    final MoDispenserMechanics pl;
    final Logger log;

    public CustomBehaviorManager(MoDispenserMechanics pl) {
        this.pl = pl;
        this.log = pl.getLogger();
    }

    public void registerCustomBehaviors() {
        FileConfiguration conf = pl.getConfig();

        // BlockPlace mechanic
        if (conf.getBoolean("mechanics.block place mechanic.enabled")) {
            log.info("registering block place mechanic");
            HashSet<Item> disabledItems = ConfigUtil.resolve(
                    conf.getStringList("mechanics.block place mechanic.disabled items"),
                    BuiltInRegistries.ITEM,
                    "block place mechanic.disabled items");

            // register BlockPlaceMechanic for all BlockItems
            BuiltInRegistries.ITEM.stream()
                    .filter(item -> item instanceof BlockItem && !disabledItems.contains(item))
                    .forEach(item -> registerMechanic(item, new BlockPlaceMechanic()));
        }

        // ToolBreakBlock mechanic
        if (conf.getBoolean("mechanics.tool break block mechanic.enabled")) {
            log.info("registering tool break block mechanic");
            ToolBreakBlockTask.speedMultiplier = (float) conf.getDouble("mechanics.tool break block mechanic.speed multiplier");
            ToolBreakBlockMechanic.disabledBlocks.clear();
            ToolBreakBlockMechanic.disabledTools.clear();

            ToolBreakBlockMechanic.disabledBlocks.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.tool break block mechanic.disabled blocks"),
                            BuiltInRegistries.BLOCK,
                            "tool break block mechanic.disabled blocks"));

            ToolBreakBlockMechanic.disabledTools.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.tool break block mechanic.disabled tools"),
                            BuiltInRegistries.ITEM, "tool break block mechanic.disabled tools"));

            ToolBreakBlockMechanic.noopOnFailure = conf.getBoolean("mechanics.tool break block mechanic.noop on failure");

            // register ToolBreakBlockMechanic for all Tools
            BuiltInRegistries.ITEM.stream()
                    .filter(ToolBreakBlockMechanic::isTool)
                    .forEach(item -> registerMechanic(item, new ToolBreakBlockMechanic()));

            ToolBreakBlockMechanic.setEnabled(true);
        } else {
            ToolBreakBlockMechanic.setEnabled(false);
        }

        // ItemUse mechanic
        if (conf.getBoolean("mechanics.item use mechanic.enabled")) {
            log.info("registering item use mechanic");
            ItemUseMechanic.interactFacingBlockFirst = conf.getBoolean("mechanics.item use mechanic.interact facing block first");
            ItemUseMechanic.interactRange = conf.getDouble("mechanics.item use mechanic.interact range");

            ItemUseMechanic.BLOCK_USE_BEHAVIORS.clear();
            ItemUseMechanic.ITEM_USE_BEHAVIORS.clear();

            ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.disabled blocks"),
                    BuiltInRegistries.BLOCK,
                    "item use mechanic.disabled blocks"
            ).forEach(b -> ItemUseMechanic.behaviorFor(b, true).disabled = true);

            ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.sneak when using on"),
                    BuiltInRegistries.BLOCK,
                    "item use mechanic.sneak when using on"
            ).forEach(b -> ItemUseMechanic.behaviorFor(b, true).sneaking = true);

            ConfigurationSection itemBehaviors = conf.getConfigurationSection("mechanics.item use mechanic.item behaviors");
            for (String k : itemBehaviors.getKeys(false)) {
                for (Item item : ConfigUtil.resolve(List.of(k), BuiltInRegistries.ITEM, "mechanics.item use mechanic.item behaviors")) {
                    ConfigurationSection behaviorConf = itemBehaviors.getConfigurationSection(k);
                    ItemUseMechanic.ItemUseBehavior behavior = ItemUseMechanic.behaviorFor(item, true);
                    behavior.enabled = behaviorConf.getBoolean("disabled", behavior.enabled);
                    behavior.preventVanilla = behaviorConf.getBoolean("prevent vanilla behavior", behavior.preventVanilla);
                    behavior.blockStateUse = behaviorConf.getBoolean("blockstate use", behavior.blockStateUse);
                    behavior.itemStackUseOn = behaviorConf.getBoolean("itemstack use on", behavior.itemStackUseOn);
                    behavior.itemStackUse = behaviorConf.getBoolean("itemstack use", behavior.itemStackUse && !(item instanceof Equipable));
                }
            }

            HashSet<Item> preventVanilla = ConfigUtil.resolve(
                    conf.getStringList("mechanics.item use mechanic.prevent vanilla behavior for"),
                    BuiltInRegistries.ITEM, "mechanics.item use mechanic.prevent vanilla behavior for");

            // register ItemUseMechanic for all ITEMS
            BuiltInRegistries.ITEM.stream()
                    .filter(item -> ItemUseMechanic.getBehaviorFor(item).enabled)
                    .forEach(item -> registerMechanic(item, new ItemUseMechanic(ItemUseMechanic.getBehaviorFor(item).preventVanilla)));
        }

        if (conf.getBoolean("mechanics.auto craft mechanic.enabled")) {
            log.info("registering auto craft mechanic");
            CachedRecipe.RECIPE_CACHE.invalidateAll();

            BuiltInRegistries.ITEM.stream()
                    .forEach(item -> registerMechanic(item, new AutoCraftMechanic()));
        }

        // WeaponAttack mechanic
        if (conf.getBoolean("mechanics.weapon attack mechanic.enabled")) {
            log.info("registering weapon attack mechanic");
            WeaponAttackMechanic.doSweeping = conf.getBoolean("mechanics.weapon attack mechanic.do sweeping attack");
            WeaponAttackMechanic.respectAttackSpeed = conf.getBoolean("mechanics.weapon attack mechanic.respect attack speed");

            WeaponAttackMechanic.DISABLED_ENTITIES.clear();
            WeaponAttackMechanic.DISABLED_ENTITIES.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.weapon attack mechanic.disabled entities"),
                            BuiltInRegistries.ENTITY_TYPE,
                            "disabled entities"));

            HashSet<Item> disabledWeapons = ConfigUtil.resolve(
                    conf.getStringList("mechanics.weapon attack mechanic.disabled weapons"),
                    BuiltInRegistries.ITEM, "weapon attack mechanic.disabled weapons");

            // register WeaponAttackMechanic for all "weapons"
            for (Item item : BuiltInRegistries.ITEM) {
                if ((item instanceof SwordItem || item instanceof AxeItem) && !disabledWeapons.contains(item)) {
                    registerMechanic(item, new WeaponAttackMechanic());
                }
            }
        }

        // AnimalBreed mechanic
        if (conf.getBoolean("mechanics.animal breed mechanic.enabled")) {
            log.info("registering animal breed mechanic");
            AnimalBreedMechanic.DISABLED_ANIMALS.clear();
            AnimalBreedMechanic.DISABLED_ANIMALS.addAll(
                    ConfigUtil.resolve(
                            conf.getStringList("mechanics.animal breed mechanic.disabled animals"),
                            BuiltInRegistries.ENTITY_TYPE,
                            "animal breed mechanic.disabled animals")
            );

            List.of(ItemTags.SNIFFER_FOOD, ItemTags.PIGLIN_FOOD, ItemTags.FOX_FOOD, ItemTags.COW_FOOD, ItemTags.GOAT_FOOD, ItemTags.SHEEP_FOOD, ItemTags.WOLF_FOOD,
                    ItemTags.CAT_FOOD, ItemTags.HORSE_FOOD, ItemTags.CAMEL_FOOD, ItemTags.ARMADILLO_FOOD, ItemTags.BEE_FOOD, ItemTags.CHICKEN_FOOD, ItemTags.FROG_FOOD,
                    ItemTags.HOGLIN_FOOD, ItemTags.LLAMA_FOOD, ItemTags.OCELOT_FOOD, ItemTags.PANDA_FOOD, ItemTags.PIG_FOOD, ItemTags.RABBIT_FOOD, ItemTags.STRIDER_FOOD,
                    ItemTags.TURTLE_FOOD, ItemTags.PARROT_FOOD, ItemTags.AXOLOTL_FOOD
            ).forEach(tag ->
                    BuiltInRegistries.ITEM.getTag(tag).ifPresent(it ->
                            it.forEach(holder ->
                                    registerMechanic(holder.value(), new AnimalBreedMechanic())
                            )
                    )
            );
        }

        // let other plugins register handlers
        Bukkit.getPluginManager().callEvent(new RegisterMechanicsEvent(this));

        // handlers are on manual init for the first time, we must initialize them
        getCustomBehaviors().forEach((k, v) -> v.init());
    }

    public Map<Item, DispenseItemHandler> getCustomBehaviors() {
        return new HashMap<>(currentHandlers);
    }

    public DispenseItemHandler getMechanicsFor(Item item) {
        if (!currentHandlers.containsKey(item)) {
            registerMechanic(item, null);
        }
        return currentHandlers.get(item);
    }

    public DispenseItemHandler registerMechanic(Item item, DispenseMechanic mechanic) {
        DispenseItemBehavior current = DispenserBlock.DISPENSER_REGISTRY.get(item);
        if (current instanceof DispenseItemHandler handler) {
            if (mechanic != null) {
                handler.add(mechanic);
            }
            return handler;
        } else {
            DispenseItemHandler handler = DispenseItemHandler.createManualInitializing(item);
            if (current != null) {
                oldBehaviorsRevert.put(item, current);
                if (current instanceof OptionalDispenseItemBehavior opt) {
                    if (current instanceof ShearsDispenseItemBehavior) {
                        // special case for handlers that don't drop the item. We can safely chain them
                        handler.add(new NoOpOptionalDispenseBehaviorWrapper(opt));
                    } else {
                        handler.add(new OptionalDispenseBehaviorWrapper(opt));
                    }
                } else {
                    handler.add(new DispenseBehaviorWrapper(current));
                }
            }
            if (mechanic != null) {
                handler.add(mechanic);
            }
            currentHandlers.put(item, handler);
            DispenserBlock.DISPENSER_REGISTRY.put(item, handler);
            return handler;
        }
    }

    public boolean unregisterMechanic(DispenseMechanic mechanic) {
        Item item = null;
        for (var entry : currentHandlers.entrySet()) {
            if (entry.getValue().contains(mechanic)) {
                item = entry.getKey();
                break;
            }
        }
        return item != null && unregisterMechanic(item, mechanic);
    }

    public boolean unregisterMechanic(Item item, DispenseMechanic mechanic) {
        DispenseItemHandler current = currentHandlers.get(item);
        if (current != null && current.remove(mechanic)) {
            if (current.isEmpty()) {
                revertBehavior(item, current);
            }
            return true;
        }
        return false;
    }

    private boolean revertBehavior(Item item, DispenseItemHandler newBehavior) {
        DispenseItemBehavior currentBehavior = DispenserBlock.DISPENSER_REGISTRY.get(item);
        DispenseItemBehavior oldBehavior = oldBehaviorsRevert.get(item);
        // check to see if any other plugins may have altered the behavior since we captured it
        if (currentBehavior == newBehavior || currentBehavior == null || currentBehavior.getClass() == DefaultDispenseItemBehavior.class) {
            newBehavior.disable();
            DispenserBlock.registerBehavior(item, oldBehavior);
            return true;
        }
        return false;
    }

    public void unregisterAllCustomBehaviors() {
        currentHandlers.forEach(this::revertBehavior);
        currentHandlers.clear();
        oldBehaviorsRevert.clear();
    }

    protected void createMechanicMetrics(Metrics metrics) {
        metrics.addCustomChart(new SingleLineChart("blocks_placed", BlockPlaceMechanic::metricsBlocksPlaced));
        metrics.addCustomChart(new SingleLineChart("blocks_broken", ToolBreakBlockTask::metricsBlocksBroken));
        metrics.addCustomChart(new SingleLineChart("items_used", ItemUseMechanic::metricsItemsUsed));
        metrics.addCustomChart(new SingleLineChart("damage_dealt", WeaponAttackMechanic::metricsDamageDealt));
        metrics.addCustomChart(new SingleLineChart("items_crafted", AutoCraftMechanic::metricsItemsCrafted));
        metrics.addCustomChart(new SingleLineChart("items_fed_to_animals", AnimalBreedMechanic::metricsItemsFedToAnimals));
    }

}
